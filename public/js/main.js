$(document).ready(function () {

    $('a.js_delete').on('click', function (e) {
        if (confirm('Are you sure?')) {
            $(this).closest('tr.js_item_area').find('input.js_delete').trigger('click');
        }
        e.preventDefault();
    })

    $(".js_close_alert").on('click', function () {
        $(this).parents(".col-xs-12").slideUp();
        return false;
    });
    $(".js_dt").datetimepicker({format: 'YYYY-MM-DD', viewMode: 'days'});
    $(".js_check").bootstrapSwitch({
        onColor : 'success',
        offColor: 'danger'
    });
    $('[title]').tooltip();
    $("[data-active]").each(function () {
        $(this).find('[value="' + $(this).data("active") + '"]').prop("selected", true);
    })
    $("[multiple]").selectpicker();
    if (window.location.hash != '' && $("[href='" + window.location.hash + "'").size() > 0)
        $("[href='" + window.location.hash + "'").trigger("click");
    $("[data-filter]").on("keyup", function () {
        var _this = $(this);
        _this.val(_this.val().replace(new RegExp(_this.data('filter'), 'ig'), ''));
    })
    $("li:has([href='" + window.location.href.split('&')[0] + "'])").addClass("active");

    $('input[name="customer[customer_phone]]"], input[name="store[store_phone]"], input[name="courier[courier_phone]"]').mask("+#999?9999999");

    $(document).on('focusout', 'input[name="code[value]"]', function () {
        var promocode = $(this).val();
        if (promocode < 0)
            $(this).val(-(promocode));
    })
})