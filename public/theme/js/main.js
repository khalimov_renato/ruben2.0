$(document).ready(function () {
    $('input, textarea').placeholder();

    $('input[type=tel]').mask("+#9999999999");


    // переход по якорям
    $('.js_anchor').on('click', function () {
        var target = $(this).attr('href')
        $('a[href^="#"]').parent('li').removeClass('active');
        $('a[href="' + target + '"]').parent('li').addClass('active');
        $('html, body').animate({scrollTop: $(target).offset().top}, 1200);
        return false;
    });

    //счетчик
    if ($('.js_coundown-block').length > 0) {
        $('.countdown').final_countdown({
            start: ($.now() + '').substring(0, 10) * 1,
            end: "1470650400",
            now: ($.now() + '').substring(0, 10) * 1,
        });
    }


    $('.datepicker').datepicker();

    /*валидация*/
    $('.js_validate [type="submit"]').on("click", function () {
        return validate($(this).parents(".js_validate"));
    });

    /*animation*/
    $('.animated').appear(function () {
        var elem = $(this);
        var animation = elem.data('animation');

        if (!elem.hasClass('visible')) {
            var animationDelay = elem.data('animation-delay');
            if (animationDelay) {
                setTimeout(function () {
                    elem.addClass(animation + " visible");
                }, animationDelay);
            } else {
                elem.addClass(animation + " visible");
            }
        }
    });

    /*btn-up*/
    $(".js_btn-up").scrollToTop();

    $('.js_checkbox').change(function () {
        if ($('.js_checkbox').is(':checked')) {
            $(this).parents('.js_remove-attr').find('input').not(this).attr('checked', false);
        }
    });

    $('.checkbox_').change(function () {
        if ($(this).is(':checked')) {
            $(this).parents('.js_remove-attr').find('.js_checkbox').attr('checked', false);
        }
    });

    $('#thank-modal').modal('show');

    function menupaint() {
        var heightWindow = $(window).outerHeight(),
            heightMenu  = $('.js_menu').outerHeight();
            if($('.js_fixed-cite').length > 0){
                var citeTop = $('.js_fixed-cite').offset().top;
            }
        $(window).on('scroll', function () {
            var scroll = $(window).scrollTop();
            if(scroll >= (heightWindow-heightMenu)) {
                $('.js_menu').addClass('bottom');
            } else {
                $('.js_menu').removeClass('bottom');
            }
            if($('.js_fixed-cite').length > 0) {
                if (scroll >= citeTop) {
                    $('.js_fixed-cite').addClass('fixed');
                } else {
                    $('.js_fixed-cite').removeClass('fixed');
                }
            }
        });
    }
    menupaint();
    $(window).on('resize', function () {
        menupaint();
    })

});
/*********************************** end document ready *******************************************/

/*-------------------------------scrolltop------------------------------*/
(function ($) {
    $.fn.scrollToTop = function () {
        $(this).hide().removeAttr("href");
        if ($(window).scrollTop() != "0") {
            $(this).fadeIn("slow")
        }
        var scrollDiv = $(this);
        $(window).scroll(function () {
            if ($(window).scrollTop() == "0") {
                $(scrollDiv).fadeOut("slow")
            } else {
                $(scrollDiv).fadeIn("slow")
            }
        });
        $(this).click(function () {
            $("html, body").animate({scrollTop: 0}, "slow")
        })
    };
})(jQuery);

/*высота блока в истории*/
function height(element) {
    $(element).each(function () {
        var height = $('.js-height-big').height() - $('.js-height-small').height() - 1;
        $(this).height(height);
    });
}


/*активности миниатюр в слайдере*/
function gallerySmallActive(element, event) {
    var parent = $(element).parents('.gallery-block');
    var page = event.page.index;
    $('.js-gallery-small .owl-item', parent).removeClass('current');
    $('.js-gallery-small .owl-item', parent).eq(page).addClass('current');
}

/*одинаковая высота блоков*/
function heightAll(el) {
    var maxH = 0;
    $(el).each(function () {
        var H_block = parseInt($(this).find('.holder').outerHeight());
        if (H_block > maxH) {
            maxH = H_block;
        }
        ;
    });
    $(el).height(maxH);
}
/*-------------------------------validate------------------------------*/
function validate(form) {
    var error_class_parent = "has-error";
    var error_class = "error";
    var norma_class = "has-success";
    var item = form.find("[required]");
    var e = 0;
    var reg = undefined;
    var pass = form.find('.password').val();
    var pass_1 = form.find('.password_1').val();
    var email = false;
    var password = false;
    var phone = false;

    function mark(object, expression) {
        if (expression) {
            object.addClass(error_class).parent('div').addClass(error_class_parent).removeClass(norma_class);
            if (email && (object.val().length > 0)) {
                object.parent('div').find('.error-text').text('Incorrect Email Format');
            }
            if (password && (object.val().length > 0)) {
                object.parent('div').find('.error-text').text('Password must be minimum 6 signs');
            }
            if (phone && (object.val().length > 0)) {
                object.parent('div').find('.error-text').text('Incorrect phone Format');
            }
            if ((object.siblings('[required]').length > 0) && (!object.siblings('[required]').hasClass(error_class))) {
                object.parent('div').addClass(norma_class).removeClass(error_class_parent);
            } else {
                object.parent('div').addClass(error_class_parent).removeClass(norma_class);
            }
            e++;
        } else
            object.removeClass(error_class).parent('div').addClass(norma_class).removeClass(error_class_parent);
    }

    form.find("[required]").each(function () {
        switch ($(this).attr("data-validate")) {
            case undefined:
                mark($(this), $.trim($(this).val()).length === 0);
                break;
            case "email":
                email = true;
                reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                mark($(this), !reg.test($.trim($(this).val())));
                email = false;
                break;
            case "phone":
                phone = true;
                reg = /^(\+)[0-9 -()]{7,}$/;
                mark($(this), !reg.test($.trim($(this).val())));
                phone = false;
                break;
            case "numb":
                reg = /[0-9]$/;
                mark($(this), !reg.test($.trim($(this).val())));
                break;
            case "pass":
                password = true;
                reg = /^[a-zA-Z0-9_-]{6,}$/;
                mark($(this), !reg.test($.trim($(this).val())));
                password = false;
                break;
            case "pass1":
                mark($(this), (pass_1 !== pass || $.trim($(this).val()).length === 0));
                break;
            default:
                reg = new RegExp($(this).attr("data-validate"), "g");
                mark($(this), !reg.test($.trim($(this).val())));
                break;
        }
    })
    form.find('.js-valid-radio').each(function () {
        var inp = $(this).find('input.required');
        var rezalt;
        for (var i = 0; i < inp.length; i++) {
            if ($(inp[i]).is(':checked') === true) {
                rezalt = 1;
                break;
            } else {
                rezalt = 0;
            }
        }
        if (rezalt === 0) {
            $(this).addClass(error_class_parent).removeClass(norma_class);
            e = 1;
        } else {
            $(this).addClass(norma_class).removeClass(error_class_parent);
        }
    })
    if (e == 0) {
        return true;
    }
    else {
        // form.find("."+error_class_parent+" input:first").focus();
        // return false;

        var top = form.find("." + error_class_parent + " .form-control:first").offset().top;
        form.find("." + error_class_parent + " .form-control:first").focus();
        $('html, body').animate({
            scrollTop: top - 110
        }, 1200);
        return false;
    }

}

/*-------------------------------PLACEHOLDER------------------------------*/
/*! http://mths.be/placeholder v2.0.7 by @mathias */
;(function (window, document, $) {

    // Opera Mini v7 doesn�t support placeholder although its DOM seems to indicate so
    var isOperaMini = Object.prototype.toString.call(window.operamini) == '[object OperaMini]';
    var isInputSupported = 'placeholder' in document.createElement('input') && !isOperaMini;
    var isTextareaSupported = 'placeholder' in document.createElement('textarea') && !isOperaMini;
    var prototype = $.fn;
    var valHooks = $.valHooks;
    var propHooks = $.propHooks;
    var hooks;
    var placeholder;

    if (isInputSupported && isTextareaSupported) {

        placeholder = prototype.placeholder = function () {
            return this;
        };

        placeholder.input = placeholder.textarea = true;

    } else {

        placeholder = prototype.placeholder = function () {
            var $this = this;
            $this
                .filter((isInputSupported ? 'textarea' : ':input') + '[placeholder]')
                .not('.placeholder')
                .bind({
                    'focus.placeholder': clearPlaceholder,
                    'blur.placeholder': setPlaceholder
                })
                .data('placeholder-enabled', true)
                .trigger('blur.placeholder');
            return $this;
        };

        placeholder.input = isInputSupported;
        placeholder.textarea = isTextareaSupported;

        hooks = {
            'get': function (element) {
                var $element = $(element);

                var $passwordInput = $element.data('placeholder-password');
                if ($passwordInput) {
                    return $passwordInput[0].value;
                }

                return $element.data('placeholder-enabled') && $element.hasClass('placeholder') ? '' : element.value;
            },
            'set': function (element, value) {
                var $element = $(element);

                var $passwordInput = $element.data('placeholder-password');
                if ($passwordInput) {
                    return $passwordInput[0].value = value;
                }

                if (!$element.data('placeholder-enabled')) {
                    return element.value = value;
                }
                if (value == '') {
                    element.value = value;
                    // Issue #56: Setting the placeholder causes problems if the element continues to have focus.
                    if (element != safeActiveElement()) {
                        // We can't use `triggerHandler` here because of dummy text/password inputs :(
                        setPlaceholder.call(element);
                    }
                } else if ($element.hasClass('placeholder')) {
                    clearPlaceholder.call(element, true, value) || (element.value = value);
                } else {
                    element.value = value;
                }
                // `set` can not return `undefined`; see http://jsapi.info/jquery/1.7.1/val#L2363
                return $element;
            }
        };

        if (!isInputSupported) {
            valHooks.input = hooks;
            propHooks.value = hooks;
        }
        if (!isTextareaSupported) {
            valHooks.textarea = hooks;
            propHooks.value = hooks;
        }

        $(function () {
            // Look for forms
            $(document).delegate('form', 'submit.placeholder', function () {
                // Clear the placeholder values so they don't get submitted
                var $inputs = $('.placeholder', this).each(clearPlaceholder);
                setTimeout(function () {
                    $inputs.each(setPlaceholder);
                }, 10);
            });
        });

        // Clear placeholder values upon page reload
        $(window).bind('beforeunload.placeholder', function () {
            $('.placeholder').each(function () {
                this.value = '';
            });
        });

    }

    function args(elem) {
        // Return an object of element attributes
        var newAttrs = {};
        var rinlinejQuery = /^jQuery\d+$/;
        $.each(elem.attributes, function (i, attr) {
            if (attr.specified && !rinlinejQuery.test(attr.name)) {
                newAttrs[attr.name] = attr.value;
            }
        });
        return newAttrs;
    }

    function clearPlaceholder(event, value) {
        var input = this;
        var $input = $(input);
        if (input.value == $input.attr('placeholder') && $input.hasClass('placeholder')) {
            if ($input.data('placeholder-password')) {
                $input = $input.hide().next().show().attr('id', $input.removeAttr('id').data('placeholder-id'));
                // If `clearPlaceholder` was called from `$.valHooks.input.set`
                if (event === true) {
                    return $input[0].value = value;
                }
                $input.focus();
            } else {
                input.value = '';
                $input.removeClass('placeholder');
                input == safeActiveElement() && input.select();
            }
        }
    }

    function setPlaceholder() {
        var $replacement;
        var input = this;
        var $input = $(input);
        var id = this.id;
        if (input.value == '') {
            if (input.type == 'password') {
                if (!$input.data('placeholder-textinput')) {
                    try {
                        $replacement = $input.clone().attr({'type': 'text'});
                    } catch (e) {
                        $replacement = $('<input>').attr($.extend(args(this), {'type': 'text'}));
                    }
                    $replacement
                        .removeAttr('name')
                        .data({
                            'placeholder-password': $input,
                            'placeholder-id': id
                        })
                        .bind('focus.placeholder', clearPlaceholder);
                    $input
                        .data({
                            'placeholder-textinput': $replacement,
                            'placeholder-id': id
                        })
                        .before($replacement);
                }
                $input = $input.removeAttr('id').hide().prev().attr('id', id).show();
                // Note: `$input[0] != input` now!
            }
            $input.addClass('placeholder');
            $input[0].value = $input.attr('placeholder');
        } else {
            $input.removeClass('placeholder');
        }
    }

    function safeActiveElement() {
        // Avoid IE9 `document.activeElement` of death
        // https://github.com/mathiasbynens/jquery-placeholder/pull/99
        try {
            return document.activeElement;
        } catch (err) {
        }
    }

}(this, document, jQuery));