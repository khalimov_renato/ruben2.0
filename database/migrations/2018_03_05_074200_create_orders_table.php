<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promocodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('promocode', 16);
            $table->integer('type')->comment('1 - personal 2 multiple');
            $table->integer('action')->comment('0 not set 1 discard delivery 2 discard comission');
            $table->integer('value');
            $table->boolean('active')->default(true);
            $table->text('description')->nullable();
            $table->dateTime('from')->nullable();
            $table->dateTime('to')->nullable();
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status')->default(0);
            $table->integer('type')->default(0);
            $table->integer('courier_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->string('addr')->nullable();
            $table->text('comment')->nullable();
            $table->double('total')->default(0);
            $table->double('surcharge')->default(0);
            $table->double('rating')->default(0);
            $table->double('start_lat')->default(0);
            $table->double('start_lng')->default(0);
            $table->double('end_lat')->default(0);
            $table->double('end_lng')->default(0);
            $table->integer('custom_place')->default(0);
            $table->integer('promocode_id')->unsigned();
            $table->dateTime('accepted_at')->nullable();
            $table->dateTime('delivered_at')->nullable();
            $table->timestamps();
            $table->foreign('promocode_id')->references('id')->on('promocodes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('courier_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('suborders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('status')->default(0);
            $table->integer('order_id')->unsigned();
            $table->text('comment')->nullable();
            $table->string('check', 128)->default(0);
            $table->double('total')->default(0);
            $table->double('comission')->default(0);
            $table->double('comission_courier')->default(0);
            $table->double('delivery')->default(0);
            $table->double('amount_min')->default(0);
            $table->double('comission_amount_min')->default(0);
            $table->foreign('order_id')->references('id')->on('orders')
                ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('suborder_product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('suborder_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->foreign('suborder_id')->references('id')->on('suborders')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suborder_product');
        Schema::dropIfExists('suborders');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('promocodes');
    }
}
