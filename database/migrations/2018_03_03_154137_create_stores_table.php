<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStoresTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('groups', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->timestamps();
                $table->softDeletes();
            });

            Schema::create('stores', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('city_id')->unsigned();
                $table->integer('group_id')->unsigned();
                $table->string('name', 128);
                $table->string('addr');
                $table->string('zip', 8);
                $table->string('logo', 32);
                $table->string('phone', 32)->nullable();
                $table->string('working');
                $table->text('description');
                $table->decimal('delivery_cost');
                $table->boolean('active')->default(false);
                $table->double('lat')->nullable();
                $table->double('lng')->nullable();
                $table->boolean('custom_order')->default(false);
                $table->string('email', 32)->nullable();
                $table->boolean('partner')->default(false);
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('city_id')->references('id')->on('cities')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('group_id')->references('id')->on('groups')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

            Schema::create('menus', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->integer('sort')->default(0);
                $table->timestamps();
                $table->softDeletes();
            });

            Schema::create('category_store', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('store_id')->unsigned();
                $table->integer('category_id')->unsigned();
                $table->foreign('store_id')->references('id')->on('stores')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('category_id')->references('id')->on('categories')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('category_store');
            Schema::dropIfExists('stores');
            Schema::dropIfExists('menus');
            Schema::dropIfExists('groups');
        }
    }
