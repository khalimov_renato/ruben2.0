<?php

    use App\Models\User\Type;
    use Illuminate\Database\Seeder;

    class TypeTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $types = ['admin', 'customer', 'courier'];
            foreach ($types as $item) {
                $type       = new Type();
                $type->name = $item;
                $type->save();
            }
        }
    }
