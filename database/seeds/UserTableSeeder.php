<?php

    use App\Models\User\User;
    use Illuminate\Database\Seeder;

    class UserTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $users = [
                [
                    'name'     => 'admin',
                    'password' => bcrypt('123456'),
                    'email'    => 'admin@example.com',
                    'active'   => 1,
                    'type_id'  => 1,
                ],
            ];
            foreach ($users as $key => $value) {
                User::create($value);
            }
        }
    }
