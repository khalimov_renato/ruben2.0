<?php

namespace App\Models\City;

use App\Models\Delivery\Delivery;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * Translations by all locales
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }
}
