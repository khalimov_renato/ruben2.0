<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    /**
     * Translations by all locales
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
