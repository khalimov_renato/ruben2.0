<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    /**
     * @return mixed
     */
    public function user()
    {
        return $this->BelongsTo(User::class);
    }
}
