<?php

namespace App\Models\Delivery;

use App\Models\City\City;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    public $timestamps = false;

    /**
     * @return mixed
     */
    public function city()
    {
        return $this->BelongsTo(City::class);
    }
}
