<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class Suborder extends Model
{
    /**
     * @return mixed
     */
    public function order()
    {
        return $this->BelongsTo(Order::class);
    }
}
