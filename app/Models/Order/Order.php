<?php

namespace App\Models\Order;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * Translations by all locales
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function suborders()
    {
        return $this->hasMany(Suborder::class);
    }

    /**
     * @return mixed
     */
    public function customer()
    {
        return $this->BelongsTo(User::class, 'customer_id');
    }

    /**
     * @return mixed
     */
    public function courier()
    {
        return $this->BelongsTo(User::class, 'courier_id');
    }
}
