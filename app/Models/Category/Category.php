<?php

namespace App\Models\Category;

use App\Models\City\City;
use App\Models\Store\Store;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories_stores()
    {
        return $this->belongsToMany(Store::class);
    }

    /**
     * @return mixed
     */
    public function city()
    {
        return $this->BelongsTo(City::class);
    }

    /**
     * Relation to parent category
     */
    public function parent()
    {
        return $this->belongsTo(self::class, 'parent');
    }
}
