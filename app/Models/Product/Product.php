<?php

namespace App\Models\Product;

use App\Models\Store\Menu;
use App\Models\Store\Store;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * @return mixed
     */
    public function store()
    {
        return $this->BelongsTo(Store::class);
    }

    /**
     * @return mixed
     */
    public function menu()
    {
        return $this->BelongsTo(Menu::class);
    }
}
