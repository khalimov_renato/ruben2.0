<?php

namespace App\Models\Store;

use App\Models\Category\Category;
use App\Models\City\City;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories_stores()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @return mixed
     */
    public function city()
    {
        return $this->BelongsTo(City::class);
    }

    /**
     * @return mixed
     */
    public function group()
    {
        return $this->BelongsTo(Group::class);
    }
}
