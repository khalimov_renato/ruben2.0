<?php

    namespace App\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;

    class CityRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'alias'    => 'unique:cities,alias,' . ($this->get('id') ? $this->get('id') : 'NULL') . ',id',
                'name'     => 'unique:cities,name,' . ($this->get('id') ? $this->get('id') : 'NULL') . ',id',
                'currency' => 'required|max:8|min:1',
            ];
        }
    }
