<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/backend/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function loginForm()
    {

        if(auth()->check()) {
            return redirect('/backend/');
        }

        return view('backend.auth.login');
    }

    public function login (Request $request)
    {
        auth()->attempt(['email' => $request->email, 'password' => $request->password]);
        if (auth()->check()) {
            // enabled/disabled user
            if(auth()->user()->active == 0) {
                auth()->logout();
                return redirect()->back()->withErrors(['error' => 'user does not exists'])->withInput();
            }
            return redirect()->guest('backend/overview');
        }
        return redirect()->back()->withErrors(['error' => 'user does not exists'])->withInput();
    }

    public function logout ()
    {
        auth()->logout();
        return redirect('/');
    }
}
