<?php

    namespace App\Http\Controllers\Backend\Order;

    use App\Models\Order\Order;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class OrderController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            if ($request->search != '') {
                // search by keyword in customer/courier
                $search = $request->search;
                $orders = Order::with(['customer', 'courier', 'products.store.location', 'custom_store.location'])
                    ->whereHas(
                        'customer',
                        function ($q) use ($search) {
                            $q->where("customer_last_name", "like", "%$search%");
                            $q->orWhere("customer_first_name", "like", "%$search%");
                            $q->orWhere("customer_email", "like", "%$search%");
                        }
                    )
                    ->orWhereHas(
                        'courier',
                        function ($q) use ($search) {
                            $q->where("courier_last_name", "like", "%$search%");
                            $q->orWhere("courier_first_name", "like", "%$search%");
                            $q->orWhere("courier_email", "like", "%$search%");
                        }
                    )
                    ->orWhere('order_id', $search)
                    ->orderBy("order_id", "DESC");
            } elseif ($request->courier) {
                // filter by courier
                $orders = Order::with(['customer', 'courier', 'products.store.location', 'custom_store.location'])
                    ->where("courier_id", "=", $request->courier)
                    ->orderBy("order_id", "DESC");
            } elseif ($request->customer) {
                // filter by customer
                $orders = Order::with(['customer', 'courier', 'products.store.location', 'custom_store.location'])
                    ->where("customer_id", "=", $request->customer)
                    ->orderBy("order_id", "DESC");
            } else {
                // select last 20 orders
                $orders = Order::with(['customer', 'courier', 'products.store.location', 'custom_store.location'])
                    ->orderBy("order_id", "DESC");
            }

            return view('backend/order/index', [
                'orders' => $orders->paginate(20),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
