<?php

namespace App\Http\Controllers\Backend\Overview;

use App\Models\Order\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OverviewController extends Controller
{
    public function index()
    {
        $overviews = Order::with([
            'products',
            'products.store',
            'products.store.location',
            'custom_store.location',
        ])
            ->where("status", "=", 2)
            ->orderBy("id", "DESC")
            ->get();
        return view('/backend/overview/index', ["overviews" => $overviews]);
    }
}
