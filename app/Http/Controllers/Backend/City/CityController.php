<?php

    namespace App\Http\Controllers\Backend\City;

    use App\Http\Requests\CityRequest;
    use App\Models\City\City;
    use App\Models\Delivery\Delivery;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class CityController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            return view('backend/city/index', [
                "cities" => City::paginate(20),
            ]);
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('backend/city/add', [
            ]);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(CityRequest $request)
        {
            $city           = new City();
            $city->name     = $request->name;
            $city->alias    = $request->alias;
            $city->currency = $request->currency;
            $city->active   = isset($request->active);
            $city->save();
            $i = 0;
            foreach ($request->delivery['shop_zip'] as $item) {
                if ($item) {
                    $delivery               = new Delivery();
                    $delivery->city_id      = $city->id;
                    $delivery->shop_zip     = $item;
                    $delivery->customer_zip = $request->delivery['customer_zip'][$i];
                    $delivery->distance     = $request->delivery['distance'][$i];
                    $delivery->cost         = $request->delivery['cost'][$i];
                    $delivery->save();
                    ++$i;
                }
            }
            return redirect('backend/city')
                ->with('success', 'The city was created successfully');
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            return view('backend/city/edit', [
                'city' => City::find($id),
            ]);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(CityRequest $request, $id)
        {
            $city           = City::find($id);
            $city->name     = $request->name;
            $city->alias    = $request->alias;
            $city->currency = $request->currency;
            $city->active   = isset($request->active);
            $city->update();
            $i = 0;
            Delivery::where('city_id', $city->id)->delete();
            foreach ($request->delivery['shop_zip'] as $item) {
                if ($item) {
                    $delivery               = new Delivery();
                    $delivery->city_id      = $city->id;
                    $delivery->shop_zip     = $item;
                    $delivery->customer_zip = $request->delivery['customer_zip'][$i];
                    $delivery->distance     = $request->delivery['distance'][$i];
                    $delivery->cost         = $request->delivery['cost'][$i];
                    $delivery->save();
                    ++$i;
                }
            }
            return redirect('backend/city')
                ->with('success', 'The city was updated successfully');
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            City::destroy($id);
            return redirect('backend/city')
                ->with('success', 'The city was deleted successfully');
        }
    }
