@extends('backend.app')

@section('content')

  <div class="container">
    <form action="{{ url('backend/promocodes/save') }}" autocomplete="off" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="code[id]" value="{{ $code->id or '' }}">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Promocode </label>
                <input class="form-control input-lg" id="code" type="text" name="code[promocode]" placeholder="" required value="{{ $code->promocode or '' }}">
              </div>
            </div>
            <div class="col-md-1">
              <label>&nbsp;</label>
              <p>
                <a href="#" id="generate_code" class="btn btn-lg btn-warning">
                  <span class="glyphicon glyphicon-repeat"></span>
                </a>
              </p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Type </label>
                <label class="col-md-12"><input type="radio" name="code[type]" @if(isset($code->type) && $code->type == 1) checked @endif value="1"> Personal</label>
                <label class="col-md-12"><input type="radio" name="code[type]" @if(isset($code->type) && $code->type == 2) checked @endif value="2"> Public</label>
              </div>
            </div>
            <div class="col-md-6">

              <div class="form-group">
                <label class="control-label">Active</label>
                <input name="code[active]" class="js_check" value="1" type="checkbox" {{ (1 == 1 ? 'checked' : '') }} >
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">Action </label>
                <select name="code[action]" data-active="{{ $code->action or 0 }}" class="form-control">
                  <option value="0">No action</option>
                  <option value="1">Discard delivery</option>
                  <option value="2">Discard commission</option>
                </select>
              </div>
            </div>
            <div class="col-md-6 js_value" @if(isset($code->action) && $code->action != 2) style="display: none" @endif>
              <label>Value</label>
              <div class="input-group">
                <input type="number" max="9" name="code[value]" value="{{ $code->value or '1' }}" class="form-control">
                <span class="input-group-addon">%</span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <label>Working</label>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">From </label>
                <input class="form-control js_dt" type="text" name="code[from]" placeholder="" value="{{ $code->from or '' }}">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label">to </label>
                <input class="form-control js_dt" type="text" name="code[to]" placeholder="" value="{{ $code->to or '' }}">
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Description</label>
            <textarea name="code[description]" rows="5" class="form-control">{{ $code->description or '' }}</textarea>
          </div>
        </div>
      </div>
      <button class="btn btn-primary btn-lg" type="submit">
        <span class="glyphicon glyphicon-save"></span>
        <strong> Save</strong>
      </button>
    </form>
  </div>
  <script>
    $(document).ready(function () {
      $("#generate_code").on('click', function () {
        var _code = '';
        var _alphabet = 'gmifqnbytvjzhckesdwraxp';
        for (i = 0; i < 4; i++)
          _code += _alphabet.split('').sort(function(){return 0.5-Math.random()}).join('').substr(10,2);
        $("#code").val(_code);
      });
      @if($code == null)
        $("#generate_code").trigger('click');
      @endif
      $('[name="code[action]"]').on('change', function () {
        var _val = $('.js_value');
        switch (parseInt($(this).val())) {
          case 0:
          case 1:
            _val.slideUp();
            break;
          case 2:
            _val.slideDown();
            break;
        }
      })
    })
  </script>
@endsection