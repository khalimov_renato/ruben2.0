@extends('backend.app')

@section('content')
  <div class="container">
    <p>
      <a href="{{ url('backend/promocodes/add') }}" class="btn btn-success">
        <span class="glyphicon glyphicon-plus-sign"></span>
        <b>Add new promocode</b>
      </a>
    </p>
    <table class="table table-hover table-striped table-bordered table-condensed">
      <thead>
      <tr>
        <th>ID</th>
        <th>Promocode</th>
        <th>Type</th>
        <th>Description</th>
        <th>Status</th>
        <th>Used</th>
        <th width="32px"></th>
      </tr>
      </thead>
      <tbody>
      @if ($codes->count() == 0)
        <tr>
          <th colspan="7"><h3 class="text-center"><em>Nothing found</em></h3></th>
        </tr>
      @endif
      @foreach ($codes as $code)
        <tr>
          <td>{{ $code->id }}</td>
          <td><b>{{ $code->promocode }}</b></td>
          <td>
            @if ($code->action == 1) Discard delivery @endif
            @if ($code->action == 2) Discard comission @endif
          </td>
          <td>{{ $code->description }}</td>
          <td>
            @if ($code->active == 1)
              <span class="label label-success">Active</span>
            @else
              <span class="label label-danger">Disabled</span>
            @endif
          </td>
          <td class="text-right">{{ $code->orders->count() }}</td>
          <td>
            <a href="{{ url('backend/promocodes/edit/'.$code->id) }}" class="btn btn-sm btn-primary">
              <span class="glyphicon glyphicon-pencil"></span>
            </a>
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>
  </div>
@endsection