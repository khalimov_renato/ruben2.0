@extends('backend.app')
@section('content')
  <div class="container-fluid">
    <form action="{{url('backend/customer')}}" autocomplete="off" method="GET">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <input class="form-control" type="text" placeholder="Type customer email/phone/name" value="{{ Request::get('search') }}" name="search">
          </div>
        </div>
        <div class="col-md-2">
          <button class="btn btn-info" type="submit"><span class="glyphicon glyphicon-search"></span>
            <strong>Find</strong></button>
        </div>
      </div>
    </form>
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
        <tr>
          <th>Name</th>
          <th width="150px">Email</th>
          <th width="100px">Phone</th>
          <th>City</th>
          <th>Address</th>
          <th width="155px"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($customers as $c)
          <tr>
            <td>{{$c->customer_last_name}} {{$c->customer_first_name}}</td>
            <td>{{$c->customer_email}}</td>
            <td class="text-right">{{$c->customer_phone}}</td>
            <td>{{$c->location->name or $c->city_name}}</td>
            <td>
              <small>{{$c->zip}} {{$c->customer_addr1}} {{$c->customer_addr2}}</small>
            </td>
            <td><a class="btn btn-info btn-sm" role="button" href="{{url('backend/order?customer='.$c->customer_id)}}"><span
                  class="glyphicon glyphicon-shopping-cart"></span> <strong>Orders</strong></a>
              <a class="btn btn-primary btn-sm" role="button" href="{{url('backend/customer/'.$c->customer_id)}}"><span
                  class="glyphicon glyphicon-pencil"></span> <strong>Edit</strong></a></td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
    <nav>
      {{ $customers->appends(['search' => Request::input('search')])->render() }}
    </nav>
  </div>
@endsection