@extends('backend.app')
@section('content')
  <div class="container">
    <form action="{{url('backend/customer/set')}}" autocomplete="off" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="customer[customer_id]" value="{{ $customer->customer_id }}">
      <div class="row">
        <div class="col-md-6">
          <h4>Contact information</h4>
          <div class="form-group">
            <label class="control-label">First name </label>
            <input class="form-control" type="text" name="customer[customer_first_name]" placeholder="Name" required
                   value="{{$customer->customer_first_name}}">
          </div>
          <div class="form-group">
            <label class="control-label">Last name </label>
            <input class="form-control" type="text" name="customer[customer_last_name]" placeholder="Name" required
                   value="{{$customer->customer_last_name}}">
          </div>
          <div class="form-group">
            <label class="control-label">Email </label>
            <input class="form-control" type="text" name="customer[customer_email]" placeholder="Email" required
                   value="{{$customer->customer_email}}">
          </div>
          <div class="form-group">
            <label class="control-label">Phone </label>
            <input class="form-control" type="text" name="customer[customer_phone]" placeholder="Phone" required
                   value="{{$customer->customer_phone}}">
          </div>
          <div class="form-group">
            <label class="control-label">City name </label>
            <input class="form-control" type="text" name="customer[city_name]" placeholder="City by customer" required
                   value="{{$customer->city_name}}">
          </div>
          <div class="form-group">
            <label class="control-label">City</label>
            <select name="customer[city]" data-active="{{$customer->city}}" class="form-control">
              <option value="" disabled selected>Not set</option>
              @foreach($cities as $city)
                <option value="{{$city->id}}">{{$city->name}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label class="control-label">Address 1</label>
            <input class="form-control" type="text" name="customer[customer_addr1]" placeholder="Address 1" required
                   value="{{$customer->customer_addr1}}">
          </div>
          <div class="form-group">
            <label class="control-label">Address 2</label>
            <input class="form-control" type="text" name="customer[customer_addr2]" placeholder="Address 2" required
                   value="{{$customer->customer_addr2}}">
          </div>
          <div class="form-group">
            <label class="control-label">ZIP</label>
            <input class="form-control" type="text" name="customer[zip]" placeholder="zip code" required
                   value="{{$customer->zip}}">
          </div>
          {{--<div class="form-group">--}}
          {{--<label class="control-label">Credit card</label>--}}
          {{--<input class="form-control" type="text" name="customer[customer_card]" placeholder="Cart" required value="{{$customer->customer_card}}">--}}
          {{--</div>--}}
          <div class="form-group">
            <div class="clearfix"></div>
            <label class="control-label">Active
              <div class="btn-group" role="group">
                <input name="customer[customer_active]" value="1"
                       type="checkbox" {{($customer->customer_active == 1 ? 'checked' : '')}} >
              </div>
            </label>
          </div>
        </div>
        <div class="col-md-6">
          <h4>Customer recalls</h4>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed">
              <thead>
              <tr>
                <th>Date</th>
                <th>Courier name</th>
                <th>Rate</th>
                <th>Order</th>
              </tr>
              </thead>
              <tbody>
              @foreach($customer->orders as $o)
                <tr>
                  <td>{{ $o->updated_at }}</td>
                  <td>
                    @if(isset($o->courier))
                      <a href="{{ url('backend/courier/'.$o->courier_id) }}">{{ $o->courier->courier_last_name }} {{ $o->courier->courier_first_name }}</a>
                    @else
                      Order not picked up
                    @endif
                  </td>
                  <td>
                    <span class="glyphicon {{ ($o->rating >= 1 ? 'glyphicon-star' :'glyphicon-star-empty') }}"></span>
                    <span class="glyphicon {{ ($o->rating >= 2 ? 'glyphicon-star' :'glyphicon-star-empty') }}"></span>
                    <span class="glyphicon {{ ($o->rating >= 3 ? 'glyphicon-star' :'glyphicon-star-empty') }}"></span>
                    <span class="glyphicon {{ ($o->rating >= 4 ? 'glyphicon-star' :'glyphicon-star-empty') }}"></span>
                    <span class="glyphicon {{ ($o->rating >= 5 ? 'glyphicon-star' :'glyphicon-star-empty') }}"></span>
                  </td>
                  <td><a href="{{ url('backend/order/'.$o->order_id) }}">{{ $o->order_id }} </a></td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <button class="btn btn-primary btn-lg" type="submit"><span class="glyphicon glyphicon-save"></span><strong>
          Save</strong></button>
    </form>
  </div>

@endsection

@section('js')
  <script>
    $("[name='customer[customer_active]']").bootstrapSwitch({
      onColor: 'success',
      offColor: 'danger'
    });
  </script>
@endsection