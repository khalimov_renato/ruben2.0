<p><b>Name </b>: {{$firstname}} {{$lastname}}</p>
<p><b>Phone </b>: {{$phone}} </p>
<p><b>Email </b>: {{$email}} </p>
<p><b>Your date of birth</b>: {{$birth}}</p>
<p><b>Which city can you work in?</b>:
@if ( $city == 1 ) Lausanne @endif
@if ( $city == 2 ) Geneva @endif
<p>
<b>During which time slots would you prefer to work? </b>: <br />
@foreach( $day as $kday => $one )
{{$kday}} ( {!! implode('; ',$one) !!} ) <br />
@endforeach
</p>
<p><b>What is your delivery background/experience? </b>: {{$experience}}</p>
<p><b>How did you hear about us? </b>: {{$source}}</p>
<p><b>Do you own a vehicle? </b>: {!! implode('; ',$vehicle) !!}</p>

<p><b>Do you have a smartphone (IOS/Android)? </b>: @if ( $iphone == 1 ) IOS @elseif( $iphone == 3 ) Android @elseif( $iphone == 2 ) No @endif </p>
<p><b>Do you have a pizza delivery box on your bike? </b>:    @if ( $box == 1 ) Yes @else No @endif</p>

<p><b>What is your nationality? </b>: {{$nationality}}</p>
<p><b>Are you eligible to work in Switzerland? </b>: {{$rightworks}}</p>

<p><b>Are you registered with the caisse de compensation? </b>: @if ( $compensation == 1 ) Yes @else No @endif </p>

<p><b>What languages can you currently speak? </b>: {!! implode('; ',$speak) !!}</p>

