<!--start-thank-modal-->
<div id="thank-modal" class="thank-modal modal" tabindex="-1" role="dialog">
    <div class="modal-wrap">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <button class="close" data-dismiss="modal"></button>
                    <img class="modal-img" src="{{asset('theme/images/head.png')}}" alt="">
                    <p class="title">{{trans('t.thanks_ttl')}}</p>
                    <p class="text-center">{{trans('t.thanks_msg')}}</p>
                    <button class="btn hvr-back-pulse" data-dismiss="modal">
                        <span>{{trans('t.thanks_ok')}}</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end-thank-modal-->