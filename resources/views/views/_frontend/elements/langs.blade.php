<ul class="lang_list">
	@foreach($list as $ln)
		<li>
			@if ( $ln != App::getLocale() )
			<a href = "{{url($ln.'/'.$alias)}}">
				{{$ln}}
			</a>
			@else
			<span class="active" >
				{{$ln}}
			</span>
			@endif
		</li>
		@endforeach
</ul>