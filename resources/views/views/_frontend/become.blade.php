@extends('layouts.site')

@section('wrapper')
<section class="form-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 taxt-right">
            <a href="/{{App::getLocale()}}" class="link-back col-md-offset-1" title="home"><span class="icon icon--back"></span></a>
            @include($folder.'.elements.langs',$list)
          </div>
        </div>
        <div class="row">
          <div class="col-lg-7 col-md-10 col-xs-12 col-lg-offset-3 col-md-offset-1">
            <strong class="pagetitle">{{trans('t.become_title')}}</strong>
            <p>{{trans('t.become_content')}}</p>
            <div class="form">
              @if(Session::has('error'))
                  @include($folder.'.elements.alert',['message' => trans('t.error')])
              @endif
              <form role="form" method="post" class="js_validate">
                {!! csrf_field() !!}
                {{-- *** ------ Name ----------- *** --}}
                <div class="form-group row form-group--personal">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label for="formName" class="label">{{trans('t.name')}}</label></div>
                    <div class="col-xs-12">
                      <div class="row">
                        <div class="col-sm-6 col-xs-12">
                          <input type="text" class="form-control" name="courier[firstname]" id="formName" required placeholder="{{trans('t.first')}}" />
                        </div>
                        <div class="col-sm-6 col-xs-12">
                          <input type="text" class="form-control" name="courier[lastname]" required placeholder="{{trans('t.last')}}" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- *** ------ Phone ------------- *** --}}
                <div class="form-group row form-group--personal">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label for="formPhone" class="label">{{trans('t.phone')}}</label></div>
                    <div class="col-xs-12">
                      <input type="tel" class="form-control" placeholder="+41" name="courier[phone]" id="formPhone" value=""  required data-validate="phone" />
                    </div>
                   {{-- <div class="hint">
                      <p>{{trans('t.hint_1')}} +447823557187</p>
                    </div>
                   --}}
                  </div>
                </div>
                {{-- *** ------ Email ------------- *** --}}
                <div class="form-group row form-group--personal">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label for="formEmail" class="label">Email</label></div>
                    <div class="col-xs-12">
                      <input type="email" class="form-control" name="courier[email]" id="formEmail" value=""  required data-validate="email" />
                    </div>
                    <div class="hint">
                      <p>{{trans('t.hint_2')}}</p>
                    </div>
                  </div>
                </div>
                <div class="form-group row form-group--personal">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label for="formDataBirth" class="label">{{trans('t.field_12')}}</label></div>
                    <div class="col-xs-12">
                      <div class="input-append date" id="dp3" data-date="01/01/1988" data-date-format="dd/mm/yyyy">
                        <input class="form-control datepicker" name="courier[birth]" type="text" placeholder="DD MM YYYY" value="">
                        <span class="add-on"><i class="icon-th"></i></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row form-group--personal">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label for="formWork" class="label">{{trans('t.field_13')}}</label></div>
                    <div class="col-xs-12 select-text">
                      <select name="courier[city]" id="formWork" required class="selectpicker">
                        <option value="">{{trans('t.choise')}}</option>
                        <option value="1">{{trans('t.city_1')}}</option>
                        <option value="2">{{trans('t.city_2')}}</option>
                      </select>
                    </div>
                  </div>
                </div>
                {{-- *** ------ Which shifts are you available to work? --- *** --}}
                <div class="form-group row form-group--personal">
                  <div class="col-xs-12"><label class="label">{{trans('t.field_1')}}</label></div>
                  <div class="col-xs-10">
                    <table cellspacing="0" class="table table-hover table-bordered">
                      <thead>
                        <tr>
                          <th>&nbsp;</th>
                          @foreach($times as $time)
                          <td>{{$time}}</td>
                          @endforeach
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($days as $dnum => $day)
                        <tr class="js_remove-attr">
                          <th><label for="monday">{{$day}}</label></th>
                          @foreach($times as $tnum => $time)
                          <td>
                            <div class="checkbox">
                              <label>
                                <input class="@if($tnum == count($times)-1)js_checkbox @else checkbox_ @endif" name="fields[day][{{$day}}][]" type="checkbox" value="{{$time}}" @if($tnum == 0) checked="checked" @endif>
                                <span class="check"></span>
                              </label>
                            </div>
                          </td>
                          @endforeach  
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                {{-- *** ------ How did you hear about us? ---------------- *** --}}
                <div class="form-group row">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label class="label">{{trans('t.field_2')}}</label></div>
                    <div class="col-xs-12">
                      @foreach( $sources as $snum => $source )
                      <div class="radio">
                        <label>
                          <input type="radio" name="fields[source]" value="{{$source}}" @if($snum == 0) checked @endif >
                          <span>{{$source}}</span>
                        </label>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
                {{-- *** ------ What is your delivery background/experience? ----- *** --}}
                <div class="form-group row">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label class="label">{{trans('t.field_3')}}</label></div>
                    <div class="col-xs-12">
                      @foreach( $experience as $enum => $exp )
                      <div class="radio">
                        <label>
                          <input type="radio" name="fields[experience]" value="{{$exp}}" @if($enum == 0) checked @endif>
                          <span>{{$exp}}</span>
                        </label>
                      </div>
                      @endforeach
                    </div>
                  </div>
                </div>
                {{-- *** ------ Do you own a vehicle? ----- *** --}}
                <div class="form-group row">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label class="label">{{trans('t.field_4')}}</label></div>
                    <div class="col-xs-12 js_remove-attr">
                      @foreach( $vehicles as $vnum => $vehicle )
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="fields[vehicle][]" value="{{$vehicle}}" @if($vnum == 0) checked @endif>
                          <span>{{$vehicle}}</span>
                        </label>
                      </div>
                      @endforeach
                    </div>
                    <div class="hint"><p>{{trans('t.hint_3')}}</p></div>
                  </div>
                </div>
                {{-- *** ------ Do you have an iPhone (at least 4s required)? ----- *** --}}
                <div class="form-group row">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label class="label">{{trans('t.field_5')}}</label></div>
                    <div class="col-xs-12">
                      <div class="radio">
                        <label>
                          <input type="radio" name="fields[iphone]" value="1" checked>
                          <span>IOS</span>
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="fields[iphone]" value="3" checked>
                          <span>Android</span>
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="fields[iphone]" value="2">
                          <span>{{trans('t.no')}}</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- *** ------ Do you have a pizza delivery box on your bike? ----- *** --}}
                <div class="form-group row">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label class="label">{{trans('t.field_6')}}</label></div>
                    <div class="col-xs-12">
                      <div class="radio">
                        <label>
                          <input type="radio" name="fields[box]" value="1" checked>
                          <span>{{trans('t.yes')}}</span>
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="fields[box]" value="2">
                          <span>{{trans('t.no')}}</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- *** ------ What is your nationality? ----- *** --}}
                <div class="form-group row">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label for="formNation" class="label">{{trans('t.field_8')}}</label></div>
                    <div class="col-xs-12">
                      <input type="text" class="form-control" name="fields[nationality]" value=""  id="formNation" required />
                    </div>
                  </div>
                </div>
                {{-- *** ------ Are you eligible to work in Switzerland?? ----- *** --}}
                <div class="form-group row">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label class="label">{{trans('t.field_9')}}</label></div>
                    <div class="col-xs-12">
                      @foreach( $rightworks as $rnum => $right )
                      <div class="radio">
                        <label>
                          <input type="radio" name="fields[rightworks]" value="{{$right}}" @if($rnum == 0) checked @endif>
                          <span>{{$right}}</span>
                        </label>
                      </div>
                      @endforeach 
                    </div>
                    {{-- <div class="hint">
                      <p>{{trans('t.hint_4')}}</p>
                    </div> --}}
                  </div>
                </div>
                {{-- *** ------ Are you registered with the caisse de compensation? ----- *** --}}
                <div class="form-group row">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label class="label">{{trans('t.field_10')}}</label></div>
                    <div class="col-xs-12">
                      <div class="radio">
                        <label>
                          <input type="radio" name="fields[compensation]" value="1" checked>
                          <span>{{trans('t.yes')}}</span>
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="fields[compensation]" value="2">
                          <span>{{trans('t.no')}}</span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                {{-- *** ------ What languages can you currently speak? ----- *** --}}
                <div class="form-group row">
                  <div class="form-group__item clearfix">
                    <div class="col-xs-12"><label class="label">{{trans('t.field_11')}}</label></div>
                    <div class="col-xs-12 js_remove-attr">
                      @foreach( $speaks as $snum => $speak )
                        @if( $snum == count($speaks) - 1 )
                          <div class="form-group row">
                            <div class="form-group__item clearfix">
                              <div class="col-xs-12"><label for="formSpeak" class="label label__copy">{{$speak}}</label></div>
                              <div class="col-xs-12">
                                <input type="text" class="form-control" name="fields[speak][]" value=""  id="formSpeak" />
                              </div>
                            </div>
                          </div>
                        @else
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" name="fields[speak][]" value="{{$speak}}" >
                              <span>{{$speak}}</span>
                            </label>
                          </div>
                        @endif
                      @endforeach
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn hvr-back-pulse">{{trans('t.submit')}}</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-xs-12 text-center copy">
          <noindex>
          <ul class="socialnetwork">
            <li>
              <a class="facebook hvr-wobble-vertical" rel="nofollow" href="https://m.facebook.com/RUBEN-Delivery-610560395761145/?ref=bookmarks" target="_blank"></a>
            </li>
            <li>
              <a class="instargam hvr-wobble-vertical" rel="nofollow" href="https://www.instagram.com/ruben_delivery/" target="_blank"></a>
            </li>
            <li>
              <a class="mailto hvr-wobble-vertical" rel="nofollow" href="mailto:info@rubendelivery.com" target="_blank"></a>
            </li>
          </ul>
          </noindex>
          &copy; <span class="font">Ruben</span>. {{trans('t.copy')}}
          <a href="{{url('/')}}/{{App::getLocale()}}/terms-and-conditions" class="link copy-link">{{trans('t.terms')}}</a>
        </div>
      </div>
    </section>
    <div class="btn-up js_btn-up"><span class="icon icon--arrow-up"></span></div>
    @if(Session::has('alert'))
       @include($folder.'.elements.modal')
    @endif
@endsection
