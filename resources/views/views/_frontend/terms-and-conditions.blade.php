@extends('layouts.site')

@section('wrapper')
<section class="form-section page-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-right">
            <a href="/{{App::getLocale()}}" class="link-back col-md-offset-1" title="home"><span class="icon icon--back"></span></a>
            @include($folder.'.elements.langs',$list)
          </div>
        </div>
        <div class="row">
          <div class="col-lg-7 col-md-10 col-xs-12 col-lg-offset-3 col-md-offset-1">
            <div class="text-center">
              <strong class="pagetitle">{{trans('t.terms_title')}}</strong>
              <strong class="subtitle">{{trans('t.terms_author')}}</strong>
            </div>
            <div class="content">
              {!! $content !!}
            </div>
          </div>
        </div>
        <div class="col-xs-12 text-center copy">
          <noindex>
          <ul class="socialnetwork">
            <li>
              <a class="facebook hvr-wobble-vertical" rel="nofollow" href="https://m.facebook.com/RUBEN-Delivery-610560395761145/?ref=bookmarks" target="_blank"></a>
            </li>
            <li>
              <a class="instargam hvr-wobble-vertical" rel="nofollow" href="https://www.instagram.com/ruben_delivery/" target="_blank"></a>
            </li>
            <li>
              <a class="mailto hvr-wobble-vertical" rel="nofollow" href="mailto:info@rubendelivery.com" target="_blank"></a>
            </li>
          </ul>
          </noindex>
          &copy; <span class="font">Ruben</span>. {{trans('t.copy')}}</div>
      </div>
    </section>
    <div class="btn-up js_btn-up"><span class="icon icon--arrow-up"></span></div>
    @if(Session::has('alert'))
       @include($folder.'.elements.modal')
    @endif
@endsection
