<strong class="title">Busy working? Intense schedule? Kids?
Exams? Or... Hangover? </strong>
<p>Don't worry, we've all been there! Forget about long waits and planning ahead!
    Let us introduce you to the life changing service, which will become your rescue buddy in any kind of situations.
    Whether you are stuck at work and have no time to go grocery shopping, whether you need to deliver mail ASAP,
    whether you can't  leave your children
    and need to run errands in town or just don't feel like cooking – let us handle it.<br /><br /></p>