<strong class="title">En général </strong>
<p>La société Ruben Sàrl (ci-après désignée « Société » ou « la Société ») – dont le
    siège est à Nyon – exploite des plateformes électroniques, notamment une
    application mobile (ci-après désignée « Application » ou « l’Application ») et un
    site internet (ci-après désigné « Site » ou « le Site »), destinées à faciliter des
    services de livraison de marchandises diverses (ci-après désignée
    « Marchandise » ou « Marchandises »). Dans ce contexte elle permet notamment
    la mise en relation de clients (ci-après désignés « Client » ou « le Client » ou
    « les Clients ») – qui achètent des Marchandises auprès d’entreprises tierces (ciaprès
    désignés « Tiers ») et de livreurs (ci-après désignés « Livreur » ou « le
    Livreur » ou « les Livreurs »).</p>
<p>Le Site de la Société est le suivant : rubendelivery.com
    En accédant au Site et en l’utilisant, les Clients et les Livreurs reconnaissent être
    liés par les présentes conditions générales, qu’ils déclarent avoir lues et
    comprises. Ils attestent être majeurs (18 ans révolus ou davantage) et avoir la
    capacité d’exercer leurs droits.</p>
<p>La Société se réserve le droit, à son entière discrétion, de modifier ou de
    changer ces conditions générales à tout moment. Il appartient aux Clients et aux
    Livreurs de les consulter régulièrement à l'adresse suivante : rubendelivery.com
    afin de se tenir informé de tout changement.</p>
<p>Les services sont décrits sur le Site ou l’Application au moment de la
    transaction. C’est cette description qui fait foi, non pas toute description publiée
    antérieurement sur le Site, l’Application ou ailleurs, sauf mention expresse
    clairement exprimée par la Société.</p>
<p>La Société s’occupe principalement des aspects de coordination, de facilitation
    et de mise en relation afin de faciliter la livraison des Marchandises achetées
    auprès de Tiers par les Clients et livrées chez eux par les Livreurs.</p>
<p>La Société a conclu différents partenariats lui permettant d’assurer l’exécution
    de ce service. Elle n’est toutefois pas responsable de la bonne exécution de leurs
    prestations par les autres parties impliquées (notamment les Livreurs et les
    Tiers), sauf mention contraire clairement exprimée par la Société.</p>
<p>Tout achat de Marchandises est conclu entre le Client et le Tiers. La livraison de
    ces Marchandises est du ressort des Livreurs. C’est donc le Tiers qui est
    responsable de la qualité de la Marchandise achetée et le Livreur qui est
    responsable de sa livraison. La Société assure pour sa part la facilitation de ces
    services et la mise en relation des Clients, Tiers et Livreurs.</p>
<strong class="title">Processus de transaction</strong>
<p>Le processus transaction pour commander des services au moyen du Site ou de
    l’Application est décrit sur ces derniers. Les références à ces processus de
    transaction sont mentionnés et décrits sur la page suivante du Site: rubendelivery.
    com</p>
<strong class="title">Prix</strong>
<p>Les prix des services indiqués sur le Site ou sur l’Application sont déterminants.
    Ils sont indiqués en francs suisses, montants nets, TVA comprise.</p>
<p>En général, le prix payé par le Client comprend (en fonction de sa commande) :
    (a) le prix de la Marchandise qui a été avancé par le Livreur, donc le montant
    que celui-ci a réglé directement auprès du Tiers ; (b) un coût additionnel de
    transaction correspondant à 10% de la valeur du prix de la Marchandise
    achetée ; (c) un montant déterminé pour la livraison, dépendant notamment de la
    distance entre le Tiers et le Client.</p>
<p>Aux prix indiqués pour les produits, prestations et services peuvent s'ajouter des
    frais accessoires liés (ex : frais de dossier).</p>
<p>Le montant total de la commande du Client (y compris tous les frais accessoires)
    est indiqué et récapitulé dans le « processus de la commande » sur le Site ou sur
    l’Application. Ce montant total est ainsi soumis à l’approbation du Client avant
    la validation de chaque commande.</p>
<strong class="title">Désistement</strong>
<p>Le Client ne peut en général pas se désister après avoir passé commande. Il doit
    donc accepter la livraison de la Marchandise et payer le montant de sa
    commande conformément aux indications mentionnées dans la section « Prix »
    ci-dessus.</p>
<p>La Société peut exceptionnellement décider de libérer un Client de son
    obligation de paiement, notamment lorsque cela n’entraîne aucun dommage
    pour elle, le Tiers et le Livreur.</p>
<strong class="title">Disponibilité</strong>
<p>La livraison des Marchandises commandées par les Clients au moyen du Site ou
    de l’Application est dépendante de la disponibilité de ces Marchandises auprès
    des Tiers. Une telle livraison n’est donc pas possible en cas de défaillance des
    Tiers ou des Livreurs. La Société ne peut en être tenue responsable, au moins
    qu’elle soit causée exclusivement par une négligence grave de cette dernière.</p>
<p>En cas d’indisponibilité d’une Marchandise dont la Société a connaissance, la
    Société informera, dans la mesure du possible, le Client de cette indisponibilité.
    Le Client ne sera alors évidemment pas tenu de payer la commande.</p>
<p>Il est rappelé que la Société n’est en aucun cas responsable de l’inexécution ou
    la mauvaise exécution des prestations qui sont du ressort des Livreurs ou des
    Tiers. Elle fera cela étant le nécessaire pour coordonner au mieux ces services et
    donc, autant que possible, satisfaire les attentes des Clients.</p>
<strong class="title">Méthodes de paiement</strong>
<p><u>Méthode de paiement admise</u> : carte de crédit. Le Client peut passer commande
    par carte de crédit.</p>
<strong class="title">Connectivité et aspects techniques</strong>
<p>La Société fait le nécessaire afin d’assurer la meilleure expérience utilisateur
    possible aux Clients. Elle s’assure notamment autant que possible que ses
    plateformes électroniques, notamment son Site et son Application, fonctionnent
    de façon appropriée.</p>
<p>Néanmoins, des problèmes techniques peuvent survenir, tels qu’interruption de
    la connexion, bugs informatiques, etc.</p>
<p>La Société n’est pas responsable de tels problèmes ni de leurs conséquences,
    sauf en cas de négligence grave de sa part. Elle fera néanmoins en sorte de
    résoudre tout problème dès que possible et de trouver des solutions afin
    d’assurer le meilleur service possible à ses Clients, notamment en cas
    d’interruption temporaire de l’accessibilité à son Site ou son Application.</p>
<strong class="title">Garantie</strong>
<p>La Société n’offre aucune garantie quant aux Marchandises achetées par les
    Clients et livrées par les Livreurs.</p>
<p>La garantie est assumée par le Tiers auprès duquel le Client a acheté la
    marchandise, cas échéant par le biais du Livreur qui lui a livré ladite
    Marchandise.</p>
<p>Le Livreur est responsable de la bonne livraison de la Marchandise et de prendre
    le soin nécessaire afin que celle-ci ne soit pas endommagée.</p>
<p>Malgré les précisions susmentionnées, la Société invite le Client à vérifier l’état
    des Marchandises livrées sans attendre pour exclure des vices de matière et de
    fabrication manifestes, ainsi que des dommages dus au transport. Conformément
    à la loi, le Client est obligé de signaler au Livreur ou au Tiers tout défaut ou anomalie affectant les marchandises livrées pour permettre à ceux-ci d’y
    remédier. Si le Client ne se conforme pas aux deux obligations susdites de
    vérification des Marchandises et de signalisation de leurs éventuels défauts ou
    anomalies, le Client perd en général son droit à la garantie (qu’il pourrait sinon
    faire valoir auprès du Tiers notamment). Il est enfin précisé que ne sont en
    général pas couverts les dommages dus à un mauvais entretien, au non-respect
    du mode d'emploi, ou à une usure normale.</p>
<strong class="title">Informatique et libertés</strong>
<p>La Société peut collecter différentes informations personnelles relatives aux
    Clients. Toutes ces données à caractère personnel seront considérées comme
    confidentielles. Elles ne pourront être vendues, données ou cédées de toute
    manière, sauf aux entreprises associées, si cela est utile ou nécessaire à
    l’exécution par la Société de ses obligations, notamment la bonne exécution ou
    livraison des produits, prestations ou services commandés.</p>
<p>Les informations personnelles des clients seront protégées adéquatement,
    notamment par un système de cryptage. Chaque Client pourra accéder à ces
    informations en tout temps, et pourra les modifier ou supprimer.</p>
<p>Les informations nécessaires à la gestion de la commande feront l’objet d’un
    traitement informatique et – comme indiqué ci-dessus – peuvent être
    communiquées à des entreprises associées dans le cadre de la gestion de la
    commande. La protection des données transmises auxdites entreprises associées
    est ensuite du ressort et de la responsabilité desdites entreprises.</p>
<strong class="title">Contact</strong>
<p>En cas de questions ou de réclamations, les Clients peuvent contacter la Société
    à l’adresse e-mail suivante : info@rubendelivery.com, ou en envoyant un
    courrier à l’adresse suivante : Schochenmühlestrasse 6 Baar 6340</p>
<strong class="title">Droit applicable et for juridique</strong>
<p>Le droit suisse est pour le surplus applicable.</p>
<p>Tout litige découlant de ou en lien avec tout achat effectué sur le présent site
    internet sera soumis aux juridictions compétentes au siège de la société (Baar).</p>