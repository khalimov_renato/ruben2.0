@extends('layouts.site')
@section('wrapper')
    <!--header-block start-->
    <section class="header-block">
        {{--<div class="container">--}}

        {{--<div class="coundown-block js_coundown-block col-lg-8 col-md-10 col-xs-12 col-center">--}}
        {{--<div class="clock row">--}}
        {{--<div class="clock-item clock-days countdown-time-value col-xs-3 animated" data-animation="rotateIn" data-animation-delay="400">--}}
        {{--<div class="wrap">--}}
        {{--<div class="inner">--}}
        {{--<div id="canvas_days" class="clock-canvas"></div>--}}
        {{--<div class="text">--}}
        {{--<p class="val">0</p>--}}
        {{--<p class="type-days type-time">{{trans('t.d')}}</p>--}}
        {{--</div>--}}
        {{--<!-- /.text --> --}}
        {{--</div>--}}
        {{--<!-- /.inner --> --}}
        {{--</div>--}}
        {{--<!-- /.wrap --> --}}
        {{--</div>--}}
        {{--<!-- /.clock-item -->--}}

        {{--<div class="clock-item clock-hours countdown-time-value col-xs-3 animated" data-animation="rotateIn" data-animation-delay="600">--}}
        {{--<div class="wrap">--}}
        {{--<div class="inner">--}}
        {{--<div id="canvas_hours" class="clock-canvas"></div>--}}
        {{--<div class="text">--}}
        {{--<p class="val">0</p>--}}
        {{--<p class="type-hours type-time">{{trans('t.h')}}</p>--}}
        {{--</div>--}}
        {{--<!-- /.text --> --}}
        {{--</div>--}}
        {{--<!-- /.inner --> --}}
        {{--</div>--}}
        {{--<!-- /.wrap --> --}}
        {{--</div>--}}
        {{--<!-- /.clock-item -->--}}

        {{--<div class="clock-item clock-minutes countdown-time-value col-xs-3 animated" data-animation="rotateIn" data-animation-delay="600">--}}
        {{--<div class="wrap">--}}
        {{--<div class="inner">--}}
        {{--<div id="canvas_minutes" class="clock-canvas"></div>--}}
        {{--<div class="text">--}}
        {{--<p class="val">0</p>--}}
        {{--<p class="type-minutes type-time">{{trans('t.m')}}</p>--}}
        {{--</div>--}}
        {{--<!-- /.text --> --}}
        {{--</div>--}}
        {{--<!-- /.inner --> --}}
        {{--</div>--}}
        {{--<!-- /.wrap --> --}}
        {{--</div>--}}
        {{--<!-- /.clock-item -->--}}

        {{--<div class="clock-item clock-seconds countdown-time-value col-xs-3 animated" data-animation="rotateIn" data-animation-delay="800">--}}
        {{--<div class="wrap">--}}
        {{--<div class="inner">--}}
        {{--<div id="canvas_seconds" class="clock-canvas"></div>--}}
        {{--<div class="text">--}}
        {{--<p class="val">0</p>--}}
        {{--<p class="type-seconds type-time">{{trans('t.s')}}</p>--}}
        {{--</div>--}}
        {{--<!-- /.text --> --}}
        {{--</div>--}}
        {{--<!-- /.inner --> --}}
        {{--</div>--}}
        {{--<!-- /.wrap --> --}}
        {{--</div>--}}
        {{--<!-- /.clock-item --> --}}
        {{--</div>--}}
        {{--<!-- /.clock --> --}}
        {{--</div>--}}
        {{--<div class="icon-list-device">--}}
        {{--<ul class="list-no text-center">--}}
        {{--<li><span class="icon icon--andr"></span></li>--}}
        {{--<li><span class="icon icon--ios"></span></li>--}}
        {{--</ul>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="heading-block text-center animated" data-animation="fadeInDown" data-animation-delay="300">
            <strong>Ruben</strong>
            {{--<span>{{trans('t.slider_title')}}</span>--}}
        </div>
    </section><!--header-block end-->
    <div class="height-screen">
        <div class="js_menu main-menu">
            <div class="container">
                <div class="text-right">
                    <ul class="nav-top text-right list-no">
                        <li><a href="#about" class="js_anchor">{{trans('t.about')}}</a></li>
                        <li><a href="#become" class="js_anchor">{{trans('t.become_title')}}</a></li>
                    </ul>
                    @include($folder.'.elements.langs',$list)
                </div>
            </div>
        </div>
    </div>

    <section id="about" class="about-block">
        <div class="container">
            <div class="row reset-list">
                <div class="col-md-5 col-xs-12 scrollflow -slide-right -opacity">
                    <div class="device-img">
                        <img src="{{asset('theme/assets/images/phone.png')}}" alt="">
                        <span class="icon-delivery"></span>
                    </div>
                </div>
                <div class="col-md-7 col-xs-12 scrollflow -slide-left -opacity">
                    <div class="description">
                        {!! $content !!}
                    </div>
                    <div class="button-line">
                        <a href="{{ $google_play_link }}" class="btn-app" target="_blank"><img src="{{asset('theme/images/android-app.png')}}" alt=""></a>
                        <a href="{{ $app_store_link }}" class="btn-app" target="_blank"><img src="{{asset('theme/images/ios-app.png')}}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--block-icons start-->
    <section class="block-icons text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <p class="cite">{{trans('t.home_cite1')}}.</p>
                </div>
                <div class="col-md-4">
                    <div class="item">
                  <span class="item__icon">
                    <img src="{{asset('theme/assets/images/ico-01.svg')}}" alt="">
                  </span>
                        <p>{{trans('t.home_step1')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                  <span class="item__icon">
                    <img src="{{asset('theme/assets/images/ico-02.svg')}}" alt="">
                  </span>
                        <p>{{trans('t.home_step2')}}</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                  <span class="item__icon">
                    <img src="{{asset('theme/assets/images/ico-03.svg')}}" alt="">
                  </span>
                        <p>{{trans('t.home_step3')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!--block-icons end-->

    <section class="block-cite">
        <div class="container text-center wrap-ctr">
            <p class="cite">{{trans('t.home_cite2')}}!</p>
        </div>
    </section>

    <section id="become" class="apply">
        <div class="container">
            <div class="row reset-list">
                <div class="col-md-7 col-xs-12 scrollflow -slide-right -opacity">
                    <div class="description">
                        <strong class="title">{{trans('t.become_title')}}</strong>
                        <p>{{trans('t.become_intro')}}</p>
                        <a href="{{url('/')}}/{{App::getLocale()}}/become" class="btn hvr-back-pulse">{{trans('t.apply')}}</a>
                    </div>
                </div>
                <div class="col-md-5 col-xs-12 pad0 scrollflow -slide-left -opacity">
                    <div class="apply-img">
                        <img src="{{asset('theme/assets/images/man.jpg')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center copy">
                <noindex>
                    <ul class="socialnetwork">
                        <li>
                            <a class="facebook hvr-wobble-vertical" rel="nofollow" href="https://m.facebook.com/RUBEN-Delivery-610560395761145/?ref=bookmarks" target="_blank"></a>
                        </li>
                        <li>
                            <a class="instargam hvr-wobble-vertical" rel="nofollow" href="https://www.instagram.com/ruben_delivery/" target="_blank"></a>
                        </li>
                        <li>
                            <a class="mailto hvr-wobble-vertical" rel="nofollow" href="mailto:info@rubendelivery.com" target="_blank"></a>
                        </li>
                    </ul>
                </noindex>
                &copy; <span class="font">Ruben</span>. {{trans('t.copy')}}
                <a href="{{url('/')}}/{{App::getLocale()}}/terms-and-conditions" class="link copy-link">{{trans('t.terms')}}</a>
            </div>
        </div>
    </section>
    <div class="btn-up js_btn-up"><span class="icon icon--arrow-up"></span></div>
@endsection