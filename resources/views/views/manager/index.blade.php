@extends('backend.app')
@section('content')
<div class="container-fluid">
	@if(session('status') != '')
		<div class="col-xs-12">
			<div class="alert alert-success">
				{{ session('status') }}
				<span class="glyphicon glyphicon-remove pull-right js_close_alert"></span>
			</div>
		</div>
	@endif

	<div class="row">
		<div class="col-md-12">
		<p><a class="btn btn-success" role="button" href="{{url('backend/manager/add')}}"><span class="glyphicon glyphicon-plus-sign"></span> <strong>Add new manager</strong></a></p>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover table-condensed">
			<thead>
				<tr>
					<th>Name </th>
					<th>Email </th>
					<th>Phone </th>
					<th width="50px">Role </th>
					<th width="50px">Active </th>
					<th width="70px"> </th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $u)
					<tr>
						<td>{{ $u->name }}</td>
						<td>{{ $u->email }}</td>
						<td>{{ $u->phone }} </td>
						<td>
							<span class="label label-{{ ($u->role_id == 1 ? 'success' : 'warning') }}">{{ $u->role->name }}</span>
						</td>
						<td class="text-center">
							@if ($u->active == 1)
								<span class="label label-success">Yes </span>
							@else
							<span class="label label-danger">No </span>
							@endif
						</td>

						<td> <a class="btn btn-primary btn-sm" role="button" href="{{ url('backend/manager/'.$u->id) }}"><span class="glyphicon glyphicon-pencil"></span> <strong>Edit</strong></a></td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{{ $users->render() }}
</div>
@endsection