@extends('backend.app')
@section('content')
  <div class="container">
    <form action="{{url('backend/manager/set')}}" autocomplete="off" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row">
        <div class="col-md-6">
          <h4>Contact information</h4>
          <div class="form-group">
            <label class="control-label">Name </label>
            <input class="form-control" type="text" name="user[name]" placeholder="Name" required>
          </div>
          <div class="form-group">
            <label class="control-label">Email </label>
            <input class="form-control" type="email" name="user[email]" placeholder="Email" required>
          </div>
          <div class="form-group">
            <label class="control-label">Password </label>
            <input class="form-control" type="password" name="user[password]" placeholder="Password" required>
          </div>
          <div class="form-group">
            <label class="control-label">Phone </label>
            <input class="form-control" type="text" name="user[phone]" placeholder="Phone" required>
          </div>
          <div class="form-group">
            <label class="control-label">Role </label>
            <select class="form-control" name="user[role_id]">
              @foreach($role as $r)
                <option value="{{ $r->id }}">{{ $r->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <div class="clearfix"></div>
            <label class="control-label">Active
              <div class="btn-group" role="group">
                <input name="user[active]" type="checkbox" value="1" checked>
              </div>
            </label>
          </div>
        </div>
      </div>
      <button class="btn btn-primary btn-lg" type="submit">
        <span class="glyphicon glyphicon-save"></span>
        <strong> Save</strong>
      </button>
    </form>
  </div>

@endsection

@section('js')
  <script>
    $("[name='manager[manager_active]']").bootstrapSwitch({
      onColor: 'success',
      offColor: 'danger'
    });
  </script>
@endsection