@extends('backend.app')
@section('content')
  <div class="container">
    <form action="{{url('backend/manager/set')}}" autocomplete="off" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="user[id]" value="{{ $user->id }}">
      <div class="row">
        <div class="col-md-6">
          <h4>Contact information</h4>
          <div class="form-group">
            <label class="control-label">Name </label>
            <input class="form-control" type="text" name="user[name]" placeholder="Name" value="{{ $user->name }}"
                   required>
          </div>
          <div class="form-group">
            <label class="control-label">Email </label>
            <input class="form-control" type="email" name="user[email]" placeholder="Email" value="{{ $user->email }}"
                   required>
          </div>
          <div class="form-group">
            <label class="control-label">Phone </label>
            <input class="form-control" type="text" name="user[phone]" value="{{ $user->phone }}" placeholder="Phone">
          </div>
          <div class="form-group">
            <label class="control-label">Role </label>
            <select class="form-control" name="user[role_id]" required>
              @foreach($role as $r)
                <option value="{{ $r->id }}" {{ ($r->id == $user->role_id ? "selected" : "") }}>{{ $r->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <div class="clearfix"></div>
            <label class="control-label">Active
              <div class="btn-group" role="group">
                <input name="user[active]" type="checkbox" value="1" {{ ($user->active == 1 ? 'checked' : '') }}>
              </div>
            </label>
          </div>
        </div>
      </div>
      <button class="btn btn-primary btn-lg" type="submit">
        <span class="glyphicon glyphicon-save"></span>
        <strong>Save</strong>
      </button>
    </form>
  </div>

@endsection

@section('js')
  <script>
    $("[name='user[active]']").bootstrapSwitch({
      onColor: 'success',
      offColor: 'danger'
    });
  </script>
@endsection