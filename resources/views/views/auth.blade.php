<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ruben</title>
  <link rel="stylesheet" href="{{asset('public/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
  <link rel="stylesheet" href="{{asset('public/css/styles.css')}}">
  <link rel="stylesheet" href="{{asset('public/css/Hero-Technology.css')}}">
  <link rel="stylesheet" href="{{asset('public/css/Material-Card.css')}}">
  <link rel="stylesheet" href="{{asset('public/css/Pretty-Header.css')}}">
  <link rel="stylesheet" href="{{asset('public/css/bootstrap-switch.min.css')}}">
</head>

<body>
   <div class="container">
        <form class="form-signin" method="post" action="/dashboard/">
            <h2 class="form-signin-heading"><img src="{{asset('public/img/logo.png')}}" alt=""></h2>
       
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong>Ошибка</strong>
            </div>
  
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong>Внимание</strong>
            </div>

            <label for="inputEmail" class="sr-only">Email </label>
            <input type="text" id="inputEmail" name="auth[email]" class="form-control" placeholder="Email address" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" name="auth[pass]" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit"><b>Login</b></button>
            <p><a href="#" data-toggle="modal" data-target="#forgot_pass">Restore password</a></p>
        </form>
    </div> <!-- /container -->


<script src="{{asset('public/js/jquery.min.js')}}"></script>
<script src="{{asset('public/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/js/bootstrap-switch.min.js')}}"></script>

    <div class="modal fade" id="forgot_pass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <form action="/dashboard/restore_pass" method="POST">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>   
    </div>
</div>

</body>
</html>


