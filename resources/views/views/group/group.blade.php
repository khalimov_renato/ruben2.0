@extends('backend.app')

@section('content')
    <div class="container">
        <div class="col-md-13 text-right">
            <p>
                <a class="btn btn-success" data-toggle="modal" href="{{ url('backend/group/add') }}">
                    <span class="glyphicon glyphicon-home"></span>
                    <strong>Add new group</strong>
                </a>
            </p>
        </div>
        <table class="table table-hover table-striped table-bordered table-condensed">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Store count</th>
                <th width="45px"></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($groups as $group)
                <tr>
                    <td>{{ $group->id }}</td>
                    <td>{{ $group->name }}</td>
                    <td class="text-right">{{ $group->stores->count() }}</td>
                    <td>
                        <a href="{{ url('backend/group/'.$group->id) }}" class="btn btn-sm btn-primary">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <nav>
            {{ $groups->render() }}
        </nav>
    </div>
@endsection