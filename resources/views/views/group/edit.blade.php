@extends('backend.app')

@section('content')
  <div class="container">
    <h3>Add menu</h3>
    <form action="{{ url('backend/group/'.$group->id) }}" method="post">
      {{ Form::token() }}
      <div class="form-group">
        <label>Group name</label>
        <input type="text" name="name" value="{{ $group->name }}" class="form-control">
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary"><b>Save</b></button>
      </div>
    </form>
  </div>
@endsection