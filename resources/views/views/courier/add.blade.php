@extends('backend.app')
@section('content')
  <div class="container">
    <form action="{{url('backend/courier/set')}}" autocomplete="off" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row">
        <div class="col-md-6">
          <h4>Contact information</h4>
          <div class="form-group">
            <label class="control-label">Last name </label>
            <input class="form-control" type="text" name="courier[courier_last_name]" placeholder="" required value="">
          </div>
          <div class="form-group">
            <label class="control-label">First name </label>
            <input class="form-control" type="text" name="courier[courier_first_name]" placeholder="" required value="">
          </div>
          <div class="form-group">
            <label class="control-label">ZIP </label>
            <input class="form-control" type="text" name="courier[courier_zip]" placeholder="ZIP code" value="">
          </div>
          <div class="form-group">
            <label class="control-label">Email </label>
            <input class="form-control" type="email" name="courier[courier_email]" placeholder="Email" required>
          </div>
          <div class="form-group">
            <label class="control-label">Password </label>
            <input class="form-control" type="password" name="courier[courier_pass]" placeholder="Password" value="" required>
          </div>
          <div class="form-group">
            <label class="control-label">City</label>
            <select name="courier[city]" class="form-control">
              @foreach($cities as $city)
                <option value="{{$city->id}}">{{$city->name}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label class="control-label">Phone </label>
            <input class="form-control" type="text" name="courier[courier_phone]" placeholder="Phone" required>
          </div>
          <div class="form-group">
            <div class="clearfix"></div>
            <label class="control-label">Active
              <div class="btn-group" role="group">
                <input name="courier[courier_active]" type="checkbox" checked value="1">
              </div>
            </label>
          </div>
        </div>
      </div>
      <button class="btn btn-primary btn-lg" type="submit"><span class="glyphicon glyphicon-save"></span><strong>
          Save</strong></button>
    </form>
  </div>

@endsection

@section('js')
  <script>
    $("[name='courier[courier_active]']").bootstrapSwitch({
      onColor: 'success',
      offColor: 'danger'
    });
  </script>
@endsection