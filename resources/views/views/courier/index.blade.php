@extends('backend.app')
@section('content')
<div class="container-fluid">
    @if(session('status') != '')
        <div class="col-xs-12">
            <div class="alert alert-success">
                {{ session('status') }}
                <span class="glyphicon glyphicon-remove pull-right js_close_alert"></span>
            </div>
        </div>
    @endif

<form action="{{url('backend/courier')}}" method="GET">
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <input name="search" class="form-control" type="text" placeholder="Type courier email/phone/name" value="{{Request::input('search')}}">
            </div>
        </div>
        <div class="col-md-1">
            <button class="btn btn-info" type="submit"><span class="glyphicon glyphicon-search"></span> <strong>Find</strong></button>
        </div>
        <div class="col-md-7 text-right"><a class="btn btn-success" role="button" href="{{url('backend/courier/add')}}"><span class="glyphicon glyphicon-plus-sign"></span> <strong>Add new courier</strong></a></div>
    </div>
</form>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
            <tr>
                <th>Name </th>
                <th>Email </th>
                <th width="150px">City </th>
                <th width="150px">Phone </th>
                <th width="100px">Raiting </th>
                <th width="50px">Orders </th>
                <th width="50px">Verified </th>
                <th width="50px">Status </th>
                <th width="160px"> </th>
            </tr>
        </thead>
        <tbody>
            @foreach($couriers as $c) 
                <tr>
                    <td>{{$c->courier_last_name}} {{$c->courier_first_name}} </td>
                    <td>{{$c->courier_email}}</td>
                    <td>{{$c->location->name or $c->city_name}}</td>
                    <td class="text-right">{{$c->courier_phone}}</td>
                    <td>
                        <?php $rating = $c->order->avg('rating') ?>
                        <span class="glyphicon glyphicon-star{{ ($rating > 0 ? "" : "-empty") }}"></span>
                        <span class="glyphicon glyphicon-star{{ ($rating > 1 ? "" : "-empty") }}"></span>
                        <span class="glyphicon glyphicon-star{{ ($rating > 2 ? "" : "-empty") }}"></span>
                        <span class="glyphicon glyphicon-star{{ ($rating > 3 ? "" : "-empty") }}"></span>
                        <span class="glyphicon glyphicon-star{{ ($rating > 4 ? "" : "-empty") }}"></span>
                    </td>
                    <td>{{ $c->order->count() }}</td>
                    <td> 
                        @if ($c->admin_verification == 1)
                            <span class="label label-success">Yes </span>
                        @else
                            <span class="label label-danger">No </span>
                        @endif
                    </td>
                    <td>
                        @if ($c->courier_active == 1)
                            <span class="label label-success">Active </span>
                        @else
                            <span class="label label-danger">Blocked </span>
                        @endif
                    </td>
                    <td> 
                        <a class="btn btn-info btn-sm" role="button" href="{{url('backend/order?courier='.$c->courier_id)}}"><span class="glyphicon glyphicon-shopping-cart"></span> <strong>Orders</strong></a>
                        <a class="btn btn-primary btn-sm" href="{{url('backend/courier/'.$c->courier_id)}}"><span class="glyphicon glyphicon-pencil"></span> <strong>Edit</strong></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<nav>
{{ $couriers->appends(['search' => Request::input('search')])->render() }}
</nav>
</div>
@endsection