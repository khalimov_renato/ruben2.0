@extends('backend.app')

@section('content')
  <div class="container">
    <table class="table table-hover table-striped table-bordered table-condensed">
      <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Products count</th>
        <th width="85px"></th>
      </tr>
      </thead>
      <tbody>
      @foreach ($menus as $menu)
        <tr>
          <td>{{ $menu->id }}</td>
          <td>{{ $menu->name }}</td>
          <td class="text-right">{{ $menu->products->count() }}</td>
          <td>
            <a href="{{ url('backend/menu/edit/'.$menu->id) }}" class="btn btn-sm btn-primary">
              <span class="glyphicon glyphicon-pencil"></span>
            </a>
            @if ($menu->products->count() == 0)
              <a href="{{ url('backend/menu/delete/'.$menu->id) }}" onclick="return confirm('Are you sure?')" class="btn btn-sm btn-danger">
                <span class="glyphicon glyphicon-remove"></span>
              </a>
            @endif
          </td>
        </tr>
      @endforeach
      </tbody>
    </table>

    <nav>
      {{ $menus->render() }}
    </nav>
  </div>
@endsection