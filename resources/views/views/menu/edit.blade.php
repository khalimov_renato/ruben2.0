@extends('backend.app')

@section('content')
  <div class="container">
    <h3>Edit menu</h3>
    <form action="{{ url('backend/menu/save/'.$menu->id) }}" method="post">
      {{ Form::token() }}
      <div class="form-group">
        <label>Menu name</label>
        <input type="text" name="name" value="{{ $menu->name }}" class="form-control">
      </div>
      <div class="form-group">
        <label>Sort order</label>
        <input type="number" name="sort" value="{{ $menu->sort }}" class="form-control">
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary"><b>Save</b></button>
      </div>
    </form>
  </div>
@endsection