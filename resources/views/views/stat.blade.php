@extends('backend.app')

@section('content')
  <div class="container-fluid">
    <div class="row">
      <form action="{{ url('backend/stat') }}" method="get">
        <div class="col-md-1">
          <div class="form-group">
            <input type="text" name="from" placeholder="Date from" class="form-control js_dt" value="{{ Request::get('from') }}">
          </div>
        </div>
        <div class="col-md-1">
          <div class="form-group">
            <input type="text" name="to" placeholder="Date to" class="form-control js_dt" value="{{ Request::get('to') }}">
          </div>
        </div>
        <div class="col-md-1">
          <button type="submit" class="btn btn-primary">
            <span class="glyphicon glyphicon-filter"></span>
            <b>Build report</b>
          </button>
        </div>
      </form>
    </div>
    <div class="panel panel-default">
      {{--<div class="panel-heading">Overall stats</div>--}}
      <div class="panel-body">
        <div class="col-md-12 text-center">
          <div class="row">
            <div class="col-md-2">
              <h3>Orders maked</h3>
            </div>
            <div class="col-md-2">
              <h3>Orders delivered</h3>
            </div>
            <div class="col-md-2">
              <h3>Customers</h3>
            </div>
            <div class="col-md-2">
              <h3>Couriers</h3>
            </div>
            <div class="col-md-2">
              <h3>Store</h3>
            </div>
            <div class="col-md-2">
              <h3>Products</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <h1 class="text-danger"><b>{{ $stat->orders }}</b></h1>
            </div>
            <div class="col-md-2">
              <h1 class="text-success"><b>{{ $stat->delivered }}</b></h1>
            </div>
            <div class="col-md-2">
              <h1 class="text-default"><b>{{ $stat->customers }}</b></h1>
            </div>
            <div class="col-md-2">
              <h1 class="text-warning"><b>{{ $stat->couriers }}</b></h1>
            </div>
            <div class="col-md-2">
              <h1 class="text-info"><b>{{ $stat->store }}</b></h1>
            </div>
            <div class="col-md-2">
              <h1 class="text-info"><b>{{ $stat->products }}</b></h1>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading"><span class="glyphicon glyphicon-shopping-cart"></span> <b>Orders</b></div>
          <div class="panel-body">

            <div id="stat_orders" class="chart">
              @if(count($orders) == 0)
                <h3 class="text-center text-muted"><i>No data</i></h3>
              @endif
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading"><span class="glyphicon glyphicon-user"></span> <b>Users registrations by type</b></div>
          <div class="panel-body">
            <div id="stat_users" class="chart"></div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading"><span class="glyphicon glyphicon-phone"></span> <b>Operation system</b></div>
          <div class="panel-body">
            <div id="os" class="chart"></div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-heading"><span class="glyphicon glyphicon-user"></span> <b>Customer by cities</b></div>
          <div class="panel-body">
            <div id="city_customers" class="chart"></div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-heading"><span class="glyphicon glyphicon-user"></span> <b>Courier by cities</b></div>
          <div class="panel-body">
            <div id="city_couriers" class="chart"></div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading"><span class="glyphicon glyphicon-user"></span> <b>Top 5 orders</b></div>
          <div class="panel-body">
            <table class="table table-bordered table-condensed table-striped table-hover">
              <thead>
              <tr>
                <th width="100px">ID</th>
                <th>Customer</th>
                <th>Cost</th>
              </tr>
              </thead>
              <tbody>
              @foreach($topOrder as $top)
                <tr>
                  <td><a href="{{ url('backend/order/'.$top->order_id) }}" target="_blank">{{ $top->order_id }}</a></td>
                  <td><a href="{{ url('backend/customer/'.$top->customer_id) }}" target="_blank">{{ $top->customer->customer_first_name }} {{ $top->customer->customer_last_name }}</a></td>
                  <td class="text-right">{{ $top->order_total }} <small class="text-muted"></small></td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading"><span class="glyphicon glyphicon-user"></span> <b>Top 5 customers</b></div>
          <div class="panel-body">
            <table class="table table-bordered table-condensed table-striped table-hover">
              <thead>
              <tr>
                <th>Customer</th>
                <th>Total orders</th>
                <th>Total commission</th>
              </tr>
              </thead>
              <tbody>
              @foreach($topCustomer as $top)
                <tr>
                  <td><a href="{{ url('backend/customer/'.$top->customer_id) }}" target="_blank">{{ $top->customer_first_name }} {{ $top->customer_last_name }}</a></td>
                  <td class="text-right">{{ $top->orders }} <small class="text-muted"></small></td>
                  <td class="text-right">{{ $top->commission }} <small class="text-muted"></small></td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-heading"><span class="glyphicon glyphicon-user"></span> <b>Top 5 couriers</b></div>
          <div class="panel-body">
            <table class="table table-bordered table-condensed table-striped table-hover">
              <thead>
              <tr>
                <th>Courier</th>
                <th>Orders</th>
              </tr>
              </thead>
              <tbody>
              @foreach($topCourier as $top)
                <tr>
                  <td><a href="{{ url('backend/courier/'.$top->courier_id) }}" target="_blank">{{ $top->courier_first_name }} {{ $top->courier_last_name }}</a></td>
                  <td class="text-right">{{ $top->orders }} <small class="text-muted"></small></td>
                </tr>
              @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
      google.charts.load('current', {'packages': ['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);


      function drawVisualization() {
          // Orders
          @if(count($orders) > 0)
          new google.visualization.ComboChart(document.getElementById('stat_orders')).draw(google.visualization.arrayToDataTable([
              ['Date', 'Total', 'Standart', 'Custom', 'Finished', 'With promocode'],
              @foreach($orders as $order)
                ['{{ $order->date }}', {{ $order->total }}, {{ $order->standart }}, {{ $order->custom }}, {{ $order->finished }}, {{ $order->promocode }}],
              @endforeach
          ]), {
              seriesType: 'bars',
              legend: {position: 'bottom'},
              animation: {duration: 800, easing: 'inAndOut', startup: true}
          });
          @endif
          // Users
          new google.visualization.ComboChart(document.getElementById('stat_users')).draw(google.visualization.arrayToDataTable([
              ['Date', 'Customers', 'Couriers'],
              @foreach($users as $user)
              ['{{ $user->date }}', {{ $user->customers or 0 }}, {{ $user->couriers or 0 }}],
            @endforeach
          ]), {
              seriesType: 'bars',
              legend: {position: 'bottom'},
              animation: {duration: 800, easing: 'inAndOut', startup: true}
          });
          // Cities by customer
          new google.visualization.PieChart(document.getElementById('city_customers')).draw(google.visualization.arrayToDataTable([
              ['City', 'Count'],
              @foreach($cities as $city)
              @if($city->type == 'customer')
              ['{{ $city->city }}',  {{ $city->count }}],
            @endif
            @endforeach
          ]), {
              is3D: true
          });
          // Cities by courier
          new google.visualization.PieChart(document.getElementById('city_couriers')).draw(google.visualization.arrayToDataTable([
              ['City', 'Count'],
              @foreach($cities as $city)
              @if($city->type == 'courier')
              ['{{ $city->city }}',  {{ $city->count }}],
            @endif
            @endforeach
          ]), {
              is3D: true
          });
          // Operation system
          new google.visualization.BarChart(document.getElementById('os')).draw(google.visualization.arrayToDataTable([
              ['OS', 'Count', {role: "style"}],
              ['Android', {{ $os->android }}, 'color:#3c763d'],
              ['iOS', {{ $os->ios }}, 'color:#c54a48']
          ]), {
              legend: {position: 'none'},
              animation: {duration: 800, easing: 'inAndOut', startup: true}
          });
      }
  </script>
@endsection