<div class="modal fade" id="edit_category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit category</h4>
      </div>
      <form action="{{ url('backend/category/save') }}" enctype="multipart/form-data" method="post">
        <input type="hidden" name="cat[cat_id]" value="">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-9">
              <div class="form-group">
                <label>Name</label>
                {{Form::text('cat[cat_name]', '', ['class' => 'form-control', 'placeholder' => 'Catalogue title'])}}
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>Position</label>
                {{Form::text('cat[position]', null, ['class' => 'form-control'])}}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Active</label>
                {{Form::checkbox('cat[cat_active]', 1, false, ['class' => 'js_check'])}}
              </div>
            </div>
            <div class="col-md-6 text-right">
              <div class="form-group">
                <label>Take away</label>
                {{Form::checkbox('cat[takeaway]', 1, false, ['class' => 'js_check'])}}
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Parent category</label>
                <select class="form-control" name="cat[cat_parent]">
                  <option value="0">Root</option>
                  @foreach($category as $cat_one)
                    <option value="{{ $cat_one['cat_id'] }}">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;{{ $cat_one['cat_name'] }}</option>
                    @foreach($cat_one['all_children'] as $cat_two)
                      <option value="{{ $cat_two['cat_id'] }}">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;{{ $cat_two['cat_name'] }}</option>
                      @foreach($cat_two['all_children'] as $cat_three)
                        <option value="{{ $cat_three['cat_id'] }}">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;{{ $cat_three['cat_name'] }}</option>
                        @foreach($cat_three['all_children'] as $cat_four)
                          <option value="{{ $cat_four['cat_id'] }}">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;{{ $cat_four['cat_name'] }}</option>
                        @endforeach
                      @endforeach
                    @endforeach
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <img class="img-thumbnail pull-right" src="" width="100">
                {{Form::file('icon', ['accept' => 'image/*'])}}
                <div class="clearfix"></div>
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">
            <span class="glyphicon glyphicon-ban-cirlce"></span>
            Cancel
          </button>
          <a href="{{ url('backend/category/delete/') }}" class="btn btn-danger js_confirm" data-confirm="Delete ?">
            <span class="glyphicon glyphicon-remove-sign"></span>
            Delete
          </a>
          <button type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-ok-sign"></span>
            Save
          </button>
        </div>
      </form>
    </div>
  </div>
</div>


<script>
  $(document).on('shown.bs.modal', '#edit_category', function () {
    $.ajax({
      type: "POST",
      dataType: "json",
      url: "{{ url('backend/category/edit') }}",
      data: "cat=" + {{$curr_cat}} +"&_token={{ csrf_token() }}",
      success: function (response) {
        $("#edit_category [name='cat[cat_name]']").val(response.cat_name);
        $("#edit_category [name='cat[position]']").val(response.position);
        $("#edit_category [name='cat[cat_id]']").val(response.cat_id);
        // delete button
        $("#edit_category .js_confirm").attr("href", $("#edit_category .js_confirm").attr("href") + "/" + response.cat_id);
        // place icon
        if (response.cat_icon == '')
          $("#edit_category img").remove();
        else
          $("#edit_category img").attr('src', response.cat_icon);

        // check active state
        if (response.cat_active == 0)
          $("#edit_category [name='cat[cat_active]']:last").bootstrapSwitch('state', false, false);
        else
          $("#edit_category [name='cat[cat_active]']:last").bootstrapSwitch('state', true, true);

        // check takeaway state
        if (response.takeaway == 0)
          $("#edit_category [name='cat[takeaway]']:last").bootstrapSwitch('state', false, false);
        else
          $("#edit_category [name='cat[takeaway]']:last").bootstrapSwitch('state', true, true);

        $("#edit_category [name='cat[cat_parent]'] [value=" + response.cat_parent + "]").prop("selected", true)
        $("#edit_category").modal('show');
      }
    })
  });
</script>