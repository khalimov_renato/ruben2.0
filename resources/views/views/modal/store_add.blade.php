<div class="modal fade" id="add_category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add new category</h4>
      </div>
      <form action="{{ url('backend/category/save') }}" enctype="multipart/form-data" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <div class="modal-body">
          <div class="form-group">
            <label>Name</label>
            {{Form::text('cat[cat_name]', '', ['class' => 'form-control', 'placeholder' => 'Catalogue title'])}}
          </div>

          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Active</label>
                {{Form::checkbox('cat[cat_active]', 1, false, ['class' => 'js_check'])}}
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Take away</label>
                {{Form::checkbox('cat[takeaway]', 1, false, ['class' => 'js_check'])}}
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Position</label>
                {{Form::text('cat[position]', null, ['class' => 'form-control'])}}
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Parent category</label>
            <select class="form-control" name="cat[cat_parent]">
              <option value="0">Root</option>
              @foreach($category as $cat_one)
                <option value="{{ $cat_one['cat_id'] }}">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;{{ $cat_one['cat_name'] }}</option>
                @foreach($cat_one['all_children'] as $cat_two)
                  <option value="{{ $cat_two['cat_id'] }}">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;{{ $cat_two['cat_name'] }}</option>
                  @foreach($cat_two['all_children'] as $cat_three)
                    <option value="{{ $cat_three['cat_id'] }}">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;{{ $cat_three['cat_name'] }}</option>
                    @foreach($cat_three['all_children'] as $cat_four)
                      <option value="{{ $cat_four['cat_id'] }}">&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;{{ $cat_four['cat_name'] }}</option>
                    @endforeach
                  @endforeach
                @endforeach
              @endforeach
            </select>
          </div>
          <div class="form-group">
            {{Form::file('icon', ['accept' => 'image/*'])}}
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning pull-left" data-dismiss="modal">
            <span class="glyphicon glyphicon-ban-cirlce"></span>
            Cancel
          </button>
          <button type="submit" class="btn btn-success">
            <span class="glyphicon glyphicon-ok-sign"></span>
            Save
          </button>
        </div>
      </form>
    </div>
  </div>
</div>