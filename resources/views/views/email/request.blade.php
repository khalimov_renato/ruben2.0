<table cellpadding="5" cellspacing="0" style="border-collapse: collapse; width:100%;" border="1">
  <tr>
    <td width="200px">Customer</td>
    <td>{{$name}}</td>
  </tr>
  <tr>
    <td>Phone</td>
    <td>{{$phone}}</td>
  </tr>
  <tr>
    <td>Email</td>
    <td>{{$email}}</td>
  </tr>
  <tr>
    <td>Request date</td>
    <td>{{$date}}</td>
  </tr>
  <tr>
    <td>Subject</td>
    <td>{{$subject}}</td>
  </tr>
  <tr>
    <td valign="top">Message</td>
    <td>{{$text}}</td>
  </tr>
</table>