<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Ruben</title>
  <style type="text/css">
    q:before {
      content: "";
    }

    q:after {
      content: "";
    }

    a {
      color: #515151 !important;
      text-decoration: none !important;
    }

    a:link {
      color: #515151 !important;
    }

    a:visited {
      color: #515151 !important;
    }

    a:active {
      color: #515151 !important;
    }

    a:hover {
      padding: 0;
      text-decoration: none !important;
    }

    p {
      font-family: Helvetica, sans-serif;
      font-size: 22px;
      line-height: 40px;
      color: #515151;

    }

    .yshortcuts {
      text-decoration: none !important;
    }

    .MsoNormal {
      margin: 0;
    }
  </style>
</head>
<body bgcolor="#ededed" color="#515151" style="background:#ededed;" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<div style="background-image:#ededed;">
  <table bgcolor="#fff" width="700" align="center" cellpadding="0" cellspacing="0">
    <tr height="32">
      <td></td>
    </tr>
    <tr>
      <td>
        <table width="600" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;vertical-align:top;">
          <tr>
            <td colspan="3" bgcolor="#fff">
              <table width="580" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;vertical-align:top;">
                <tr>
                  <td>
                    <table width="580" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;vertical-align:top;">
                      <tr>
                        <td height="92" colspan="3">
                          <p style="text-align:center;"><img src="http://rubendelivery.com/theme/images/ruben.png"></p>
                        </td>
                      </tr>
                      <tr>
                        <td height="18" colspan="3">
                          <p style="text-align:center; font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">
                            Merci pour votre commande!
                          </p>
                        </td>
                      </tr>
                      <tr>
                        <td height="92" colspan="3">
                          <p style="text-align:center;"><img src="http://rubendelivery.com/theme/images/circle.png"></p>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="3">
                          <p style="text-align:center; font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">
                            Merci d'avoir choisi RUBEN! <br/> Vous trouverez le récapitulatif de votre commande ci-dessous.
                          </p>
                        </td>
                      </tr>
                      <tr>
                        <td height="30" colspan="3"></td>
                      </tr>
                      @foreach($custom as $cu)
                        <tr>
                          <td height="50" colspan="3">
                            <p style="text-align:center; font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">
                              Détails de votre commande personnalisée #{{$cu['id']}} :
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td height="73">
                            <p style="font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">
                              {{$cu['store']}}
                            </p>
                          </td>
                        </tr>
                        <tr>
                          <td height="73">
                            <p style="font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">
                              {{$cu['text']}}
                            </p>
                          </td>
                        </tr>
                      @endforeach
                      @if(0 < count($store))
                        <tr>
                          <td height="50" colspan="3">
                            <p style="text-align:center; font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">Les détails de votre commande #{{$order_id}} :</p>
                          </td>
                        </tr>
                      @endif
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td width="0" valign="top" height="0" style="vertical-align:top;line-height:0; font-size:0;">
            </td>
            <td bgcolor="#ffffff" width="600">
              <table align="center" width="580" cellpadding="0" cellspacing="0" style="border-collapse:collapse;vertical-align:top;">
                <tr>
                  <td>
                  </td>
                </tr>
                @if(0 < count($store))
                  @foreach($store as $key => $val)
                    <tr>
                      <td height="73">
                        <p style="width: 355px; font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">{{$key}}</p>
                      </td>
                    </tr>
                    <tr>
                      <td valign="top"></td>
                    </tr>
                    @foreach($val as $product)
                      <tr style="border-top: 1px solid #eeeeee;" height="150">
                        <td align="left">
                          <p style="width: 355px; font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">
                            {{ $product['name'] }}
                            @if ($product['count'] > 1)
                              x {{ $product['count'] }}
                            @endif
                          </p>
                        </td>
                        <td align="right">
                          <p style="font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">
                            {{$currency}} {{$product['price']}}
                          </p>
                        </td>
                      </tr>
                    @endforeach
                  @endforeach
                @endif
                <tr style="border-top: 1px solid #eeeeee;">
                  <td align="left">
                    <p style="font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">Le prix de livraison</p>
                  </td>
                  <td align="right"><p style=" font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">{{$currency}} {{$order_delivery}}</p></td>
                </tr>
                <tr>
                  <td align="left">
                    <p style="font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;"> Supplément jusqu'à l'ordre de {{ \App\Http\Helpers::showSetting('amount_min') }} CHF</p>
                  </td>
                  <td align="right">
                    <p style="font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">
                      {{$currency}} {{ \App\Http\Helpers::showSetting('commission_amount_min') }}
                    </p>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    <p style="font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">Service</p>
                  </td>
                  <td align="right">
                    <p style="font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">
                      {{$currency}} {{$order_comission}}
                    </p>
                  </td>
                </tr>
                <tr>
                  <td align="left"><p></p></td>
                  <td align="right">
                    <p style=" font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">
                      TOTALE: {{$currency}} {{$order_total}}
                    </p>
                  </td>
                </tr>
                <tr height="55">
                  <td></td>
                </tr>
                <tr>
                  <td align="left">
                    <p style="font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">L'adresse de livraison</p>
                  </td>
                  <td align="right">
                    <p style="font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">
                      {{$delivery}}
                    </p>
                  </td>
                </tr>
                <tr height="70">
                  <td></td>
                </tr>
              </table>
              <table align="center" width="377" cellpadding="0" cellspacing="0" style="border-collapse:collapse;vertical-align:top;">
                <tr style="border-top: 2px solid #f4d4e2;">
                  <td height="120" colspan="3">
                    <p style="text-align:center; font-family: Helvetica, sans-serif;font-size: 22px;line-height: 40px;color: #515151;">Suivez nous sur<br/><a href="http://rubendelivery.com"
                                                                                                                                                           target="_blank">rubendelivery.com</a></p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p style="text-align:center;">
                      <span>
                        <a href="https://m.facebook.com/RUBEN-Delivery-610560395761145/?ref=bookmarks" target="_blank">
                          <img src="http://rubendelivery.com/theme/images/fb_min.png" style="margin-right: 16px;">
                        </a>
                      </span>
                      <span>
                        <a href="https://www.instagram.com/ruben_delivery/" target="_blank">
                          <img src="http://rubendelivery.com/theme/images/ins_min.png">
                        </a>
                      </span>
                    </p>
                  </td>
                </tr>
                <td height="92" colspan="3">
                  <p style="text-align:center;"><img src="http://rubendelivery.com/theme/images/circle_big.png"></p>
                </td>
                </tr>
                <tr>
                  <td height="92" colspan="3">
                    <p style="text-align:center;"><img src="http://rubendelivery.com/theme/images/ruben.png"></p>
                  </td>
                </tr>
              </table>
          </tr>
          <tr height="32">
            <td></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</div>
</body>
</html>