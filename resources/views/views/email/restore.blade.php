<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Ruben</title>
  <style type="text/css">
    q:before{
      content:"";
    }
    q:after{
      content:"";
    }
    a{
      color:#201e1f!important;
    }
    a:link{
      color:#201e1f!important;
    }
    a:visited{
      color:#201e1f!important;
    }
    a:active{
      color:#201e1f!important;
    }
    a:hover{
      padding:0;
      text-decoration:none!important;
    }
    .yshortcuts {
      text-decoration: none!important;
    }
    .MsoNormal{
      margin:0;
    }
  </style>
</head>
<body bgcolor="#ededed" style="background:#ededed;" marginheight="0" marginwidth="0" leftmargin="0" topmargin="0">
<div style="background-image:#ededed;">
  <table width="600" align="center" cellpadding="0" cellspacing="0">
    <tr height="32" style="line-height:0; font-size:0;">
      <td></td>
    </tr>
    <tr>
      <td style="line-height:0; font-size:0;">
        <table width="600" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;vertical-align:top;">
          <tr>
            <td colspan="3" bgcolor="#f4d4e3">
              <table width="600" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;vertical-align:top;">
                <tr>
                  <td>
                    <table width="600" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;vertical-align:top;">
                      <tr>
                        <td height="92" colspan="3" style="line-height:0; font-size:0;">
                          <table width="600" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse;vertical-align:top;">
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="1" bgcolor="#acacac">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td width="0" valign="top" height="0" style="vertical-align:top;line-height:0; font-size:0;">
            </td>
            <td bgcolor="#ffffff" width="600" style="line-height:0; font-size:0;">
              <table align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse;vertical-align:top;">
                <tr height="20" style="line-height:0; font-size:0;">
                  <td></td>
                </tr>
                <table align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse;vertical-align:top;">
                  <tr height="20" style="line-height:0; font-size:0;">
                    <td></td>
                  </tr>
                  <tr>
                    <td width="81px" rowspan="3"></td>
                    <td valign="top">
                        <span style="font-family: Helvetica, sans-serif;color:#201e1f;font-size:22px;line-height:50px;color:#515151;">
												<p>Hello {{ $name }}!</p>
												<p>
												Below you will find a new password <br/>
												to access your account. </p>
												<p>
												Email: {{$email}}
												<br/>
												Password: <span style="color:#201e1f;">{{ $pass }}</p>
												<p style="margin-top:45px;">Sincerely yours, <br/>Ruben Delivery </p>
												<p><img src="http://rubendelivery.com/img/ruben.png" class="logo"></p>
												<p><a href="http://rubendelivery.com">http://rubendelivery.com</a></p>
                    </td>
                  </tr>

                  <tr height="150" style="line-height:0; font-size:0;">
                    <td></td>
                  </tr>
                </table>
                <tr height="32" style="line-height:0; font-size:0;">
                  <td></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</div>
</body>
</html>