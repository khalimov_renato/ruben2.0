<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Ruben</title>
  <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-theme.min.css')}}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
  <link rel="stylesheet" href="{{asset('css/styles.css')}}">
  <link rel="stylesheet" href="{{asset('css/Hero-Technology.css')}}">
  <link rel="stylesheet" href="{{asset('css/Material-Card.css')}}">
  <link rel="stylesheet" href="{{asset('css/Pretty-Header.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-switch.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-datetimepicker.min.css')}}">
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/bootstrap-switch.min.js')}}"></script>
  <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
  <script src="{{asset('js/moment.min.js')}}"></script>
  <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
  <script src="{{asset('js/jquery.maskedinput.js')}}"></script>
  <script src="{{asset('js/main.js')}}"></script>
</head>
<body>
@yield('js')
<nav class="navbar navbar-default custom-header">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand navbar-link" href="{{ url('backend/overview') }}" style="padding: 0px">
        <img src="{{ asset('img/logo_white.png') }}" height="50px" alt="">
      </a>
      <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="navbar-collapse">
      <ul class="nav navbar-nav links">
        <li @if(Request::is('*overview*')) class="active" @endif role="presentation"><a href="{{ url('backend/overview') }}">Overview </a></li>
        <li @if(Request::is('*order*')) class="active" @endif role="presentation"><a href="{{ url('backend/order') }}">Orders </a></li>
        <li @if(Request::is('*city*')) class="active" @endif role="presentation"><a href="{{ url('backend/city') }}">Cities </a></li>
        <li @if(Request::is('*store*')) class="active" @endif role="presentation"><a href="{{ url('backend/store') }}"> Catalogue</a></li>
        <li @if(Request::is('*group*')) class="active" @endif role="presentation"><a href="{{ url('backend/group') }}"> Store groups</a></li>
        <li @if(Request::is('*menu*')) class="active" @endif role="presentation"><a href="{{ url('backend/menu') }}"> Menu</a></li>
        <li @if(Request::is('*promocode*')) class="active" @endif role="presentation"><a href="{{ url('backend/promocodes') }}"> Promocodes</a></li>
        <li @if(Request::is('*courier*')) class="active" @endif role="presentation"><a href="{{ url('backend/courier') }}">Couriers </a></li>
        <li @if(Request::is('*customer*')) class="active" @endif role="presentation"><a href="{{ url('backend/customer') }}"> Customers</a></li>
        <li @if(Request::is('*manager*')) class="active" @endif role="presentation"><a href="{{ url('backend/manager') }}">Managers</a></li>
        <li @if(Request::is('*stat*')) class="active" @endif role="presentation"><a href="{{ url('backend/stat') }}">Statistic</a></li>
      </ul>
      <ul class="nav navbar-nav links navbar-right">
        <li @if(Request::is('*setting*')) class="active" @endif role="presentation"><a href="{{ url('backend/setting')}} ">Settings </a></li>
        <li role="presentation"><a href="{{ url('backend/logout') }}">Logout </a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="container">
  @if(session('status') != '')
    <div class="row">
      <div class="col-xs-12">
        <div class="alert alert-success">
          {{ session('status') }}
          <span class="glyphicon glyphicon-remove pull-right js_close_alert"></span>
        </div>
      </div>
    </div>
  @endif
  @if(session('error') != '')
    <div class="row">
      <div class="col-xs-12">
        <div class="alert alert-danger">
          {{ session('error') }}
          <span class="glyphicon glyphicon-remove pull-right js_close_alert"></span>
        </div>
      </div>
    </div>
  @endif
</div>
@yield('content')
</body>
</html>