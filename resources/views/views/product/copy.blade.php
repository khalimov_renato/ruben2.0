@extends('backend.app')

@section('content')
  <div class="container">
    <form action="" method="post" autocomplete="off">

      @if(session('message') != '')
        <div class="row">
          <div class="col-xs-12">
            <div class="alert alert-success">
              {!! session('message') !!}
              <span class="glyphicon glyphicon-remove pull-right js_close_alert"></span>
            </div>
          </div>
        </div>
      @endif
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row">
        <div class="col-md-4">
          <label>Store from</label>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-4">
          <label>Copy to</label>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <input type="number" name="from" required placeholder="ID comma separated" class="form-control">
          </div>
        </div>
        <div class="col-md-1">
          <span class="glyphicon glyphicon-arrow-right"></span>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <input type="text" name="to" required placeholder="ID" class="form-control">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-lg btn-primary"><b>Copy</b></button>
          </div>
        </div>
      </div>
    </form>
  </div>
@endsection