@extends('backend.app')

@section('content')
  <div class="container-fluid">
    <div class="col-md-2">
      <p>
        <a class="btn btn-block btn-success" data-toggle="modal" href="#add_category">
          <span class="glyphicon glyphicon-plus-sign"></span>
          <strong>Add new category</strong>
        </a>
      </p>
      @if($curr_cat != '')
        <p>
          <a class="btn btn-block btn-warning" data-cat="{{ $curr_cat }}" data-toggle="modal" href="#edit_category">
            <span class="glyphicon glyphicon-edit"></span>
            <strong>Edit this category</strong>
          </a>
        </p>
      @endif
      <ul id="catalog">
        @foreach($category as $cat_one)
          <li>
            <a href="{{ url("backend/store?category=".$cat_one['cat_id']) }}">{{ $cat_one['cat_name'] }}</a>
            @if(count($cat_one['all_children']) > 0)
              <ul>
                @foreach($cat_one['all_children'] as $cat_two)
                  <li>
                    <a href="{{ url("backend/store?category=".$cat_two['cat_id']) }}">{{ $cat_two['cat_name'] }}</a>
                    @if(count($cat_two['all_children']) > 0)
                      <ul>
                        @foreach($cat_two['all_children'] as $cat_three)
                          <li>
                            <a
                                    href="{{ url("backend/store?category=".$cat_three['cat_id']) }}">{{ $cat_three['cat_name'] }}</a>
                            @if(count($cat_three['all_children']) > 0)
                              <ul>
                                @foreach($cat_three['all_children'] as $cat_four)
                                  <li>
                                    <a
                                            href="{{ url("backend/store?category=".$cat_four['cat_id']) }}">{{ $cat_four['cat_name'] }}</a>
                                    @if(count($cat_four['all_children']) > 0)
                                      <ul>
                                        @foreach($cat_four['all_children'] as $cat_five)
                                          <li>
                                            <a
                                                    href="{{ url("backend/store?category=".$cat_five['cat_id']) }}">{{ $cat_five['cat_name'] }}</a>
                                          </li>
                                        @endforeach
                                      </ul>
                                    @endif
                                  </li>
                                @endforeach
                              </ul>
                            @endif
                          </li>
                        @endforeach
                      </ul>
                    @endif
                  </li>
                @endforeach
              </ul>
            @endif
          </li>
        @endforeach
      </ul>
    </div>
    <div class="col-md-10">
      <div class="row">
        <div class="col-md-4">
          <form action="" method="get" autocomplete="off" class="form-inline">
            <input type="hidden" name="category" value="{{ $_GET['category'] or 0 }}">
            <div class="form-group">
              <input type="text" name="search" value="{{ $_GET['search'] or '' }}" placeholder="Search store by name" class="form-control">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-info">
                <span class="glyphicon glyphicon-search"></span>
              </button>
            </div>
          </form>
        </div>
          <form action="{{ url('backend/store/change/') }}" method="get" autocomplete="off" class="form-inline">
          <div class="col-md-3">
              <div class="form-group">
                  <select class="form-control" name="change" id="change">
                      <option>Select an action</option>
                      <option value="1">delete</option>
                      <option value="2">active</option>
                      <option value="3">deactivate</option>
                  </select>
              </div>
              <div class="form-group">
                  <button type="submit"  id="button_change" class="btn btn-info">
                      <span class="glyphicon glyphicon-pencil"></span>
                  </button>
              </div>
          </div> </form>
        <div class="col-md-5 text-right">
          <p>
            <a class="btn btn-success" data-toggle="modal" href="{{ url('backend/store/add') }}">
              <span class="glyphicon glyphicon-home"></span>
              <strong>Add new store</strong>
            </a>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <table class="table table-condensed table-striped table-bordered">
            <thead>
            <tr>
              <th width="20"></th>
              <th width="32px">ID</th>
              <th width="50px">Icon</th>
              <th>Name</th>
              <th>City</th>
              <th width="50px">Delivery cost</th>
              <th width="50px">Status</th>
              <th width="50px">Custom order</th>
              <th width="50px">Products count</th>
              <th width="50px">Partner</th>
              <th width="32px"></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($stores as $store)
              <tr @if (is_array($cheked_ids) && in_array($store->store_id, $cheked_ids))
                  style="background-color: rgb(255, 221, 221);"
                  class="clicked_check"
              @endif >
                <td>
                  <div class="checkbox">
                    <label><input type="checkbox"
                                  @if (is_array($cheked_ids) && in_array($store->store_id, $cheked_ids))
                                          checked
                                  @endif
                                  class="js_check_order" value="{{ $store->store_id }}"></label>
                  </div>
                </td>
                <td><small class="text-muted">{{ $store->store_id }}</small></td>
                <td>
                  @if($store->store_logo == '')
                    <img src="{{asset('img/nophoto.png')}}" alt="" class="store_logo">
                    <img src="{{asset('img/nophoto.png')}}" alt="" class="store_logo">
                  @else
                    <img src="/cache/store/{{$store->store_id.'/'.$store->store_logo}}" alt="" class="store_logo">
                  @endif
                </td>
                <td>{{ $store->store_name }}</td>
                <td>{{ $store->location->name }}</td>
                <td class="text-right">{{ $store->store_delivery_cost }}</td>
                <td class="text-center">
                  @if($store->store_active)
                    <span class="label label-success">Active</span>
                  @else
                    <span class="label label-danger">Disabled</span>
                  @endif
                </td>
                <td class="text-center">
                  @if($store->custom_order)
                    <span class="glyphicon glyphicon-ok text-success"></span>
                  @else
                    <span class="glyphicon glyphicon-remove text-danger"></span>
                  @endif
                </td>
                <td class="text-right">{{ $store->products->count() }}</td>
                <td class="text-center">
                  @if($store->partner)
                    <span class="glyphicon glyphicon-ok text-success"></span>
                  @else
                    <span class="glyphicon glyphicon-remove text-danger"></span>
                  @endif
                </td>
                <td>
                  <a href="{{ url('backend/store/'.$store->store_id) }}" class="btn btn-primary">
                    <span class="glyphicon glyphicon-pencil"></span>
                  </a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <nav>
            {!! $stores->appends(['category' => Request::get('category'), 'search' => Request::get('search')])->render() !!}
          </nav>
        </div>
      </div>
    </div>

  </div>

  @include('modal.store_add')
  @include('modal.store_edit')

@endsection

@section('js')
  <script>
      $(".js_check").bootstrapSwitch({
          onColor: 'success',
          offColor: 'danger'
      });

      $(".js_confirm").on("click", function () {
          return (!confirm($(this).attr("data-confirm")))
      });
      $(document).ready(function () {
          if ($(".clicked_check").size() == 0)
              $("#change, #button_change").attr('disabled', 'disabled');
          else {
              $("#change, #button_change").removeAttr('disabled');
          }
          $(".js_check_order").on("click", function () {
              var _status;
              var _this = $(this).parents('tr:first');
              _this.toggleClass('clicked_check');
              if ($(".clicked_check").size() == 0)
                  $("#change, #button_change").attr('disabled', 'disabled');
              else
                  $("#change, #button_change").removeAttr('disabled');

              if (_this.hasClass('clicked_check')){
                  _status = 1;
                  _this.css('background-color','#ffdddd');
              }else{
                  _this.css('background-color','');
                  _status = 0;
              }
              $.ajax({
                  url: 'http://ruben.loc/backend/store/check/'+$(this).val()+'/'+_status,
                  type: "GET",
                  dataType: "json",
                  success: function (response) {

                  }
              })
          });
      })

  </script>
@endsection