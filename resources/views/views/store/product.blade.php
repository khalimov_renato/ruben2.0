<div class="modal fade" id="product_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add new product</h4>
      </div>
      <form autocomplete="off" action="{{ url('backend/product/save') }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="product[store_id]" value="{{$store->store_id or ''}}">
        <div class="modal-body">
          <div class="form-group">
            <label>Name</label>
            <input type="text" placeholder="Product name" value="" class="form-control" name="product[product_name]" required>
          </div>
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label>Menu</label>
                <input type="text" placeholder="Menu section" value="" required class="form-control js_autocomplete" name="product[product_menu]">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Price</label>
                <div class="input-group">
                  <input type="text" name="product[product_price]" min="1" step="0.01" max="9999.99" placeholder="Product price" value="" class="form-control text-right">
                  <span class="input-group-addon">{{$store->location->currency or ''}}</span>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label"> Active </label>
                <div class="btn-group" role="group">
                  <input name="product[product_active]" class="js_switch" value="1" type="checkbox" checked>
                </div>
              </div>
            </div>
            <div class="col-md-6 text-right">
              <div class="form-group">
                <label class="control-label"> Adult product (18+) </label>
                <div class="btn-group" role="group">
                  <input name="product[product_18plus]" class="js_switch" value="1" type="checkbox">
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea name="product[product_description]" rows="5" class="form-control"></textarea>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="product_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit product</h4>
      </div>
      <form autocomplete="off" action="{{ url('backend/product/update') }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
        <input type="hidden" name="page" value="{{ Request::input('page') }}">
        <input type="hidden" name="product[store_id]" value="">
        <input type="hidden" name="product[product_id]" value="">
        <div class="modal-body">
          <div class="form-group">
            <label>Name</label>
            <input type="text" placeholder="Product name" value="" class="form-control" name="product[product_name]" required>
          </div>
          <div class="row">
            <div class="col-md-8">
              <div class="form-group">
                <label>Menu</label>
                <input type="text" placeholder="Menu section" value="" class="form-control js_autocomplete" name="product[product_menu]">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Price</label>
                <div class="input-group">
                  <input type="text" name="product[product_price]" min="1" step="0.01" max="9999.99" placeholder="Product price" value="" class="form-control text-right">
                  <span class="input-group-addon js_currency"></span>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label"> Active </label>
                <div class="btn-group" role="group">
                  <input name="product[product_active]" class="js_switch" value="1" type="checkbox" checked>
                </div>
              </div>
            </div>
            <div class="col-md-6 text-right">
              <div class="form-group">
                <label class="control-label"> Adult product (18+) </label>
                <div class="btn-group" role="group">
                  <input name="product[product_18plus]" class="js_switch" value="1" type="checkbox" checked>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>Description</label>
            <textarea name="product[product_description]" rows="5" class="form-control"></textarea>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success"><b>Save product</b></button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="{{asset('js/bootstrap-typeahead.min.js')}}"></script>
<script>
  $(document).ready(function () {
    // switch
    $(".js_switch").bootstrapSwitch({
      onColor: 'success',
      offColor: 'danger'
    });

    // autocomplete
    $(".js_autocomplete").typeahead({
      source: [
        @foreach($menu as $m)
        {id: {{htmlspecialchars($m['id'])}}, name: "{!! htmlspecialchars($m['name']) !!}"},
        @endforeach
      ]
    });

    //edit product
    $(".js_product_edit").on("click", function () {
      $.ajax({
        url: $(this).attr("href"),
        type: "GET",
        dataType: "json",
        success: function (response) {
          $("#product_edit [name='product[store_id]']").val(response.store_id);
          $("#product_edit [name='product[product_id]']").val(response.product_id);
          $("#product_edit [name='product[product_name]']").val(response.product_name);
          $("#product_edit [name='product[product_price]']").val(response.product_price);
          if (response.menu_id !== null)
            $("#product_edit [name='product[product_menu]']").val(response.menu.name);
          else
            $("#product_edit [name='product[product_menu]']").val('');

          $("#product_edit [name='product[product_description]']").val(response.product_description);
          $("#product_edit .js_currency").html(response.store.location.currency);

          if (response.product_active == 0)
            $("#product_edit [name='product[product_active]']:last").bootstrapSwitch('state', false, false);
          else
            $("#product_edit [name='product[product_active]']:last").bootstrapSwitch('state', true, true);

          if (response.product_18plus == 0)
            $("#product_edit [name='product[product_18plus]']:last").bootstrapSwitch('state', false, false);
          else
            $("#product_edit [name='product[product_18plus]']:last").bootstrapSwitch('state', true, true);

          //$("#product_edit .js_image").attr("src", response.product_image);
          $("#product_edit").modal('show');
        }
      })
      return false;
    });

  })
</script>