@extends('backend.app')

@section('content')
  <div class="container">
    @if($store->store_active == 0)
      <div class="row">
        <div class="col-xs-12">
          <div class="alert alert-warning">
            <strong>Warning</strong> This store is not active
            <span class="glyphicon glyphicon-remove pull-right js_close_alert"></span>
          </div>
        </div>
      </div>
    @endif
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active">
        <a href="#shop" aria-controls="home" role="tab" data-toggle="tab">Shop</a>
      </li>
      <li role="presentation">
        <a href="#products" aria-controls="profile" role="tab" data-toggle="tab">Products</a>
      </li>
    </ul>
    <br>
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="shop">
        {!! Form::open(array('url' => 'backend/store/'.$store->store_id, 'method' => 'POST', 'autocomplete' => 'off', "enctype" => "multipart/form-data")) !!}
        <div class="row">
          <div class="col-md-3">
            <div id="gmap" style="width:100%;height:320px"></div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Lat</label>
                  <div class="form-group">
                    {{Form::text('store[store_lat]', $store->store_lat,array("class" => "form-control", "placeholder" => "Shop Address") )}}
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Lng</label>
                  <div class="form-group">
                    {{Form::text('store[store_lng]', $store->store_lng,array("class" => "form-control", "placeholder" => "") )}}
                  </div>
                </div>
              </div>
            </div>
            @if(!empty($store->store_logo))
              <img class="img-thumbnail pull-right"
                   src="{{asset('img/stores/'.$store->store_id."/".$store->store_logo)}}" width="100">
            @endif
            <div class="form-group">
              <label>Logo</label>
              {{Form::file('image')}}
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Name</label>
              {{Form::text('store[store_name]', $store->store_name,array("class" => "form-control", "required") )}}
            </div>
            <div class="form-group">
              <label>Address</label>
              {{Form::text('store[store_addr]', $store->store_addr,array("class" => "form-control", "placeholder" => "", "required") )}}
            </div>
            <div class="form-group">
              <label>Email</label>
              {{Form::email('store[email]', $store->email,array("class" => "form-control", "placeholder" => "") )}}
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Group</label>
                  {{Form::select('store[group_id]', $groups, $store->group_id, array("class" => "form-control", "placeholder" => "Group") )}}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>ZIP</label>
                  {{Form::text('store[zip]', $store->zip, array("class" => "form-control", "required") )}}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Phone</label>
                  {{Form::text('store[store_phone]', $store->store_phone ,array("class" => "form-control") )}}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Categories</label>
                  {{Form::select('categories[]', $categories, $store->category()->get()->pluck('cat_id')->toArray(), array("class" => "form-control", "multiple" => "multiple", "data-live-search" => "true", "required") )}}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">City</label>
                  {{Form::select('store[store_city]', $cities, $store->store_city, array("class" => "form-control", "placeholder" => "City", "required") )}}
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Description</label>
              {{Form::textarea('store[store_desc]', $store->store_desc ,array("class" => "form-control", "placeholder" => "Shop description", "size" => "30x5") )}}
            </div>
          </div>
          <div class="col-md-5">

            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <label class="control-label"> Active </label>
                  <div class="btn-group" role="group">
                    {{Form::checkbox('store[store_active]', 1, $store->store_active, ['class' => 'js_switch'])}}
                  </div>
                </div>
              </div>
              <div class="col-md-7">
                <div class="form-group">
                  <label class="control-label"> Custom order </label>
                  <div class="btn-group" role="group">
                    {{Form::checkbox('store[custom_order]', 1, $store->custom_order, ['class' => 'js_switch'])}}
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <table id="js_timetable" class="table table-condensed table-stripped table-bordered timetable">
                <thead>
                <tr>
                  <th rowspan="2">
                    <a href="#" id="js_clone" class="btn btn-xs btn-block btn-success" title="Clone Sunday timetable">
                      <span class="glyphicon glyphicon-menu-hamburger"></span>
                      <b>clone</b>
                    </a>
                  </th>
                  <th rowspan="2">Open</th>
                  <th rowspan="2">Close</th>
                  <th colspan="2">Break</th>
                </tr>
                <tr>
                  <th>From</th>
                  <th>To</th>
                </tr>
                </thead>
                <tbody>
                @for($i = 0; $i < 7; $i++)
                  <tr>
                    <td>
                      <b class="text-warning">{{$days[$i]}}</b>
                    </td>
                    <td>
                      <div class="form-group">
                        {{
                        Form::text(
                          'tt[work][start][]',
                          isset($timetable->work->start[$i]) ? $timetable->work->start[$i] :'',
                          [ 'class' => 'form-control text-center', 'maxlength' => 5, 'data-filter' => '[^\d{2}\-]+']
                        )
                        }}
                      </div>
                    </td>
                    <td>
                      {{
                      Form::text(
                        'tt[work][end][]',
                        isset($timetable->work->end[$i]) ? $timetable->work->end[$i] : '',
                        ['class' => 'form-control text-center', 'maxlength' => 5, 'data-filter' => '[^\d{2}\-]+']
                       )
                      }}
                    </td>
                    <td>
                      {{
                      Form::text(
                          'tt[break][start][]',
                          isset($timetable->break->start[$i]) ? $timetable->break->start[$i] : '',
                          ['class' => 'form-control text-center', 'maxlength' => 5, 'data-filter' => '[^\d{2}\-]+']
                        )
                      }}
                    </td>
                    <td>
                      {{
                      Form::text(
                        'tt[break][end][]',
                        isset($timetable->break->end[$i]) ? $timetable->break->end[$i] : '',
                        ['class' => 'form-control text-center', 'maxlength' => 5, 'data-filter' => '[^\d{2}\-]+']
                       )
                      }}
                    </td>
                  </tr>
                @endfor
                </tbody>
              </table>
            </div>
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <label class="control-label"> Partner </label>
                  <div class="btn-group" role="group">
                    {{Form::checkbox('store[partner]', 1, $store->partner, ['class' => 'js_switch'])}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-12">
            <button type="submit" class="btn btn-lg btn-primary">
              <span class="glyphicon glyphicon-save"></span>
              <b>Save</b>
            </button>
            <a target="_blank" href="{{ url('backend/store/clone/'.$store->store_id) }}"
               class="btn btn-lg btn-primary">
              <span class="glyphicon glyphicon-duplicate"></span>
              <strong>Clone</strong>
            </a>
          </div>
        </div>
        {!! Form::close() !!}
      </div>
      <div role="tabpanel" class="tab-pane " id="products">
        <div class="row">
          <div class="col-md-10">
            <form action="{{ url('backend/store/'.$store->store_id."#products") }}" method="GET">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Search by product name or description" name="search" value="{{ Request::input("search") }}">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <select name="menu" data-active="{{ Request::input('menu') }}" class="form-control">
                      <option value="">All</option>
                      @foreach($current_menu as $cmenu)
                        <option value="{{ $cmenu->menu_id }}">{{ $cmenu->menu->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-1">
                  <button class="btn btn-info" type="submit"><span
                            class="glyphicon glyphicon-search"></span><strong> Search </strong>
                  </button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-2">
            <p>
              <a href="#product_add" data-toggle="modal" class="btn btn-success pull-right">
                <span class="glyphicon glyphicon-plus-sign"></span>
                <b>Add new product</b>
              </a>
            </p>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover table-condensed ">
            <thead>
            <tr>
              {{--<th width="100px">Image</th>--}}
              <th>Name</th>
              <th>Description</th>
              <th width="100px">Menu</th>
              <th width="100px">Price</th>
              <th width="90px">Status</th>
              <th width="30px">Adult</th>
              <th width="70px"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $product)
              <tr>
                {{--<td>--}}
                {{--<img class="img-thumbnail" src="{{ asset('img/products/'.$product->product_id."/".$product->product_image) }}" width="100">--}}
                {{--</td>--}}
                <td>{{ $product['product_name'] }}</td>
                <td>
                  <small>{{ $product->product_description }}</small>
                </td>
                <td>{{ $product->menu->name or '' }}</td>
                <td class="text-right"><b>{{ $product->product_price }}</b> {{ $store->location->currency  }}</td>
                <td class="text-center">
                  @if($product->product_active == 1)
                    <span class="label label-success">Available </span>
                  @else
                    <span class="label label-danger">Not available </span>
                  @endif
                </td>
                <td class="text-center">
                  @if($product->product_18plus == 1)
                    <span class="label label-success">Yes </span>
                  @else
                    <span class="label label-danger">No</span>
                  @endif
                </td>
                <td>
                  <a href="{{url('backend/product/edit/'.$product->product_id)}}"
                     class="btn btn-sm btn-primary js_product_edit">
                    <span class="glyphicon glyphicon-pencil"></span>
                    <strong>Edit</strong>
                  </a>

                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <nav>
          {{ $items->appends(['search' => Request::input("search"), 'menu' => Request::input("menu")])->fragment('products')->render() }}
        </nav>
      </div>
    </div>
  </div>

  @include('store.product')

@endsection

@section('js')
  <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_KEY')}}" defer></script>
  <script>

      $(document).ready(function () {

          var map = new google.maps.Map(document.getElementById('gmap'), {
              center: new google.maps.LatLng({{ $store->store_lat }},{{ $store->store_lng }}),
              zoom: 14
          });
          var marker = new google.maps.Marker({
              position: new google.maps.LatLng({{ $store->store_lat }},{{ $store->store_lng }}),
              map: map
          });

          map.addListener('click', function (e) {
              marker.setPosition(e.latLng);
              $("[name='store[store_lat]']").val(e.latLng.lat());
              $("[name='store[store_lng]']").val(e.latLng.lng());
          });

          $('#js_clone').on("click", function () {
              var values = [];

              $('#js_timetable tbody tr:first input').each(function () {
                  values.push($(this).val());
              })

              $('#js_timetable tbody tr:gt(0)').each(function () {
                  $(this).find("input").each(function () {
                      $(this).val(values[$(this).parents('td').index() - 1]);
                  })
              })
              return false;
          })

      });
      // tab products
      if (document.location.hash != '') {
          $("[href='" + document.location.hash + "']").trigger("click");
      }

  </script>
@endsection