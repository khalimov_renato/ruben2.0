@extends('backend.app')

@section('content')
  <div class="container">
    @if(session('status') != '')
      <div class="col-xs-12">
        <div class="alert alert-success">
          {{ session('status') }}
          <span class="glyphicon glyphicon-remove pull-right js_close_alert"></span>
        </div>
      </div>
    @endif
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#shop" aria-controls="home" role="tab" data-toggle="tab">Shop</a>
      </li>
      <li role="presentation"><a href="#products" aria-controls="profile" role="tab" data-toggle="tab">Products</a></li>
    </ul>
    <br>
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="shop">
        {!! Form::open(array('url' => 'backend/store/save', 'method' => 'POST', 'autocomplete' => 'off', "enctype" => "multipart/form-data")) !!}
        <div class="row">
          <div class="col-md-3">
            <div id="gmap" style="width:100%;height:320px"></div>
            <div class="row">
              <div class="col-md-6">
                <label>Lat</label>
                <div class="form-group">
                  {{Form::text('store[store_lat]', null, array("class" => "form-control", "placeholder" => "", "required") )}}
                </div>
              </div>
              <div class="col-md-6">
                <label>Lng</label>
                <div class="form-group">
                  {{Form::text('store[store_lng]', null,array("class" => "form-control", "placeholder" => "", "required") )}}
                </div>
              </div>
            </div>
            @if(!empty($store->store_logo))
              <img class="img-thumbnail pull-right" src="" width="100">
            @endif
            <div class="form-group">
              <label>Logo</label>
              {{Form::file('image')}}
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Name</label>
              {{Form::text('store[store_name]', null,array("class" => "form-control", "placeholder" => "", "required") )}}
            </div>
            <div class="form-group">
              <label>Address</label>
              {{Form::text('store[store_addr]', null,array("class" => "form-control", "placeholder" => "", "required") )}}
            </div>
            <div class="form-group">
              <label>Email</label>
              {{Form::email('store[email]', null,array("class" => "form-control", "placeholder" => "") )}}
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label">Group</label>
                  {{Form::select('store[group_id]', $groups, null, array("class" => "form-control", "placeholder" => "Group") )}}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>ZIP</label>
                  {{Form::text('store[zip]', null, array("class" => "form-control", "required", "required") )}}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Phone</label>
                  {{Form::text('store[store_phone]', null ,array("class" => "form-control", "placeholder" => "") )}}
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Description</label>
              {{Form::textarea('store[store_desc]', null ,array("class" => "form-control", "placeholder" => "", "size" => "30x5") )}}
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category</label>
                  {{Form::select('categories[]', $categories, null, array("class" => "form-control", "multiple" => "multiple", "required") )}}
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">City</label>
                  {{Form::select('store[store_city]', $cities, null, array("class" => "form-control", "placeholder" => "City", "required") )}}
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5">

            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <label class="control-label"> Active </label>
                  <div class="btn-group" role="group">
                    {{Form::checkbox('store[store_active]', 1, 1, ['class' => 'js_switch'])}}
                  </div>
                </div>
              </div>
              <div class="col-md-7">
                <div class="form-group">
                  <label class="control-label"> Custom order </label>
                  <div class="btn-group" role="group">
                    {{Form::checkbox('store[custom_order]', 1, 1, ['class' => 'js_switch'])}}
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <table id="js_timetable" class="table table-condensed table-bordered timetable">
                <thead>
                <tr>
                  <th rowspan="2">
                    <a href="#" id="js_clone" class="btn btn-xs btn-block btn-success" title="Clone Sunday timetable">
                      <span class="glyphicon glyphicon-menu-hamburger"></span>
                      <b>clone</b>
                    </a>
                  </th>
                  <th rowspan="2">Open</th>
                  <th rowspan="2">Close</th>
                  <th colspan="2">Break</th>
                </tr>
                <tr>
                  <th>From</th>
                  <th>To</th>
                </tr>
                </thead>
                <tbody>
                @for($i = 0; $i < 7; $i++)
                  <tr>
                    <td>
                      <b class="text-warning">{{$days[$i]}}</b>
                    </td>
                    <td>
                      <div class="form-group">
                        {{ Form::text('tt[work][start][]','',['class' => 'form-control text-center', 'maxlength' => 5, 'data-filter' => '[^\d{2}\-]+']) }}
                      </div>
                    </td>
                    <td>{{ Form::text('tt[work][end][]','',['class' => 'form-control text-center', 'maxlength' => 5, 'data-filter' => '[^\d{2}\-]+']) }}</td>
                    <td>{{ Form::text('tt[break][start][]','',['class' => 'form-control text-center', 'maxlength' => 5, 'data-filter' => '[^\d{2}\-]+']) }}</td>
                    <td>{{ Form::text('tt[break][end][]','',['class' => 'form-control text-center', 'maxlength' => 5, 'data-filter' => '[^\d{2}\-]+']) }}</td>
                  </tr>
                @endfor
                </tbody>
              </table>
            </div>
            <div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <label class="control-label"> Partner </label>
                  <div class="btn-group" role="group">
                    {{Form::checkbox('store[partner]', 1, 0, ['class' => 'js_switch'])}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-12">
            <button type="submit" class="btn btn-lg btn-primary">
              <span class="glyphicon glyphicon-save"></span>
              <b>Save</b>
            </button>
          </div>
        </div>
        {!! Form::close() !!}
      </div>
      <div role="tabpanel" class="tab-pane " id="products">
        <div class="row">
          <div class="col-md-6">
            <form action="{{ url('backend/store/save#products') }}" method="GET">
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Search by product name or description"
                           name="search" value="{{ Request::input("search") }}">
                  </div>
                </div>
                <div class="col-md-3">
                  <button class="btn btn-info" type="submit"><span
                            class="glyphicon glyphicon-search"></span><strong> Search </strong>
                  </button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-6">
            <p>
              <a href="#product_add" data-toggle="modal" class="btn btn-success pull-right">
                <span class="glyphicon glyphicon-plus-sign"></span>
                <b>Add new product to this catalog</b>
              </a>
            </p>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
            <tr>
              <th width="100px">Image</th>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              <th width="90px">Status</th>
              <th width="70px"></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <nav>
        </nav>
      </div>
    </div>
  </div>
  @include('store.product')
@endsection

@section('js')
  <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_KEY')}}" defer></script>
  <script>

      $(document).ready(function () {

          var map = new google.maps.Map(document.getElementById('gmap'), {
              center: new google.maps.LatLng(48.69096039092549, 8.96484375),
              zoom: 3
          });
          var marker = new google.maps.Marker({
              position: new google.maps.LatLng(48.69096039092549, 8.96484375),
              map: map
          });

          map.addListener('click', function (e) {
              marker.setPosition(e.latLng);
              $("[name='store[store_lat]']").val(e.latLng.lat());
              $("[name='store[store_lng]']").val(e.latLng.lng());
          });

          $('#js_clone').on("click", function () {
              var values = [];

              $('#js_timetable tbody tr:first input').each(function () {
                  values.push($(this).val());
              })

              $('#js_timetable tbody tr:gt(0)').each(function () {
                  $(this).find("input").each(function () {
                      $(this).val(values[$(this).parents('td').index() - 1]);
                  })
              })
              return false;
          })
      });

      // tab products
      if (document.location.hash != '') {
          $("[href='" + document.location.hash + "']").trigger("click");
      }

  </script>
@endsection

