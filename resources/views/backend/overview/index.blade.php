@extends('backend.app')
@section('content')
    <div class="row">

        <div class="col-md-6 col-sm-12 col-xs-12">
            <div id="gmap" style="height:600px;width:100%"></div>

        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <table class="table table-bordered table-condensed table-hover">
                <thead>
                <tr>
                    <th class="text-center" width="50px">Order ID</th>
                    <th>Customer</th>
                    <th>Courier</th>
                    <th>Address</th>
                    <th class="text-center" width="70px">Order cost</th>
                    <th width="32px"></th>
                </tr>
                </thead>
                <tbody>
                <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_KEY')}}" defer></script>
                <script>
                    direction = [];
                    points    = [];
                </script>
                @forelse($overviews as $o)
                    <tr>
                        <td><a href="{{ url('backend/order/'.$o->id) }}">{{ $o->id }}</a></td>
                        <td>
                            <a href="{{ url('backend/customer/'.$o->customer->id) }}">
                                {{ $o->customer->last_name }} {{ $o->customer->first_name }}
                            </a>
                        </td>
                        <td>
                            @if (!is_null($o->courier))
                                <a href="{{ url('backend/courier/'.$o->courier->id) }}">
                                    {{ $o->courier->last_name }} {{ $o->courier->first_name }}
                                </a>
                            @endif
                        </td>
                        <td>
                            <small>{{ $o->order_addr }}</small>
                        </td>
                        <td class="text-right">
                            <b>{{ $o->total + $o->comission + $o->comission_courier + $o->delivery }}</b>
                            <small class="text-muted">
                                @if(isset($o->custom_store))
                                    {{ $o->custom_store->location->currency }}
                                @else
                                    {{ $o->products[0]->store->location->currency }}
                                @endif
                            </small>
                        </td>
                        <td>
                            @if (!is_null($o->courier))
                                <a href="#" class="btn btn-sm btn-info js_dir"
                                   data-courier-lat="{{ $o->courier->courier_lat }}"
                                   data-courier-lng="{{ $o->courier->courier_lng }}">
                                    <span class="glyphicon glyphicon-map-marker"></span>
                                </a>
                            @endif
                        </td>
                    </tr>

                    <script>
                        poo   = [];
                        var d = {
                            from: {
                                lat: {{ $o->order_start_lat }},
                                lng: {{ $o->order_start_lng }},
                            },
                            too : {
                                lat: {{ $o->order_end_lat }},
                                lng: {{ $o->order_end_lng }},
                            }
                        };
                        direction.push(d);
                                @foreach($o->products as $product)
                        var p = {
                            location: {
                                lat     : {{ $product->store->store_lat }},
                                lng     : {{ $product->store->store_lng }},
                                stopover: true,
                                //info: new google.maps.InfoWindow({content: {{ $product->store->store_name }} })
                            },
                        };
                        poo.push(p);
                        @endforeach
                        points.push(poo);
                    </script>
                @empty
                    <tr>
                        <td colspan="7" class="warning"><h3 class="text-center">There is no order in progress now</h3></td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('js')

    <script>
        var map, row, directionsService, directionsDisplay;
        var mrk = [];

        function initMap() {
        }

        /**
         * coord {from{lat,lng}, to{lat,lng}} старт конец
         * points [lat,lng]  координаты промежуточных точек
         */
        function draw_route(coord, points) {
            directionsService.route({
                origin           : {lat: parseFloat(coord.from.lat), lng: parseFloat(coord.from.lng)},
                destination      : {lat: parseFloat(coord.too.lat), lng: parseFloat(coord.too.lng)},
                optimizeWaypoints: true,
                waypoints        : points,
                travelMode       : google.maps.TravelMode.DRIVING
            }, function (response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });
        }

        $(document).ready(function () {
            // created map
            map        = new google.maps.Map(document.getElementById('gmap'), {
                center: {lat: 47.376867, lng: 8.552143},
                zoom  : 14
            });
            var marker = '';

            // directection
            directionsService = new google.maps.DirectionsService;
            directionsDisplay = new google.maps.DirectionsRenderer;
            directionsDisplay.setMap(map);

            //button direction
            $(".js_dir").on("click", function () {
                var c_lat = $(this).attr("data-courier-lat");
                var c_lng = $(this).attr("data-courier-lng");
                if (marker == '') {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(c_lat, c_lng),
                        title   : 'Hello World!',
                        icon    : "{{ asset('img/courier.png') }}"
                    });
                    marker.setMap(map);
                } else {
                    marker.setPosition(new google.maps.LatLng(c_lat, c_lng));
                }
                var _index = (parseInt($(this).closest("tr").index()) / 2) - 1;
                draw_route(direction[_index], points[_index]);
                return false;
            });

        });
    </script>
@endsection