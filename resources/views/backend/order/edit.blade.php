@extends('backend.app')
@section('content')
    <div class="container">

        <form action="{{ url('backend/order/set') }}" autocomplete="off" method="POST">

            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <input type="hidden" name="order[order_id]" value="{{ $order->order_id }}">
            <div class="row">
                <div class="col-md-4">
                    <h4>Order information</h4>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Customer </h3></div>
                        <div class="panel-body">
                            <p>
                                <a href="{{ url('backend/customer/'.$order->customer_id) }}">
                                    {{ $order->customer->customer_last_name }} {{ $order->customer->customer_first_name }}
                                </a>
                            </p>
                            <p>{{ $order->customer->customer_phone }}</p>
                            <p>{{ $order->customer->customer_email }} </p>
                            <p>{{ $order->customer->customer_addr }}</p>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Courier</h3></div>
                        <div class="panel-body">
                            @if(isset($order->courier))
                                <p>
                                    <a href="{{ url('backend/courier/'.$order->courier->courier_id) }}">
                                        {{ $order->courier->courier_last_name }} {{ $order->courier->courier_first_name }}
                                    </a>
                                </p>
                                <p>{{ $order->courier->courier_phone }}</p>
                                <p>{{ $order->courier->courier_email }}</p>
                            @else
                                <p class="text-danger">
                                    Order not picked up yet
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"></div>
                    <div class="form-group"></div>
                    <div class="form-group">
                        <label class="control-label">Order status</label>
                        <select class="form-control" name="order[order_status]">
                            <option value="1" {{ ($order->order_status == 1 ? 'selected' : "") }}>New</option>
                            <option value="2" {{ ($order->order_status == 2 ? 'selected' : "") }}>In progress</option>
                            <option value="0" {{ ($order->order_status == 0 ? 'selected' : "") }}>Cancelled</option>
                            <option value="3" {{ ($order->order_status == 3 ? 'selected' : "") }}>Delivered</option>
                            <option value="4" {{ ($order->order_status == 4 ? 'selected' : "") }}>Picked Up</option>
                            <option value="5" {{ ($order->order_status == 5 ? 'selected' : "") }}>Collected</option>
                            <option value="6" {{ ($order->order_status == 6 ? 'selected' : "") }}>On the way</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="clearfix"></div>
                    </div>
                </div>
                @if ($order->order_type == 0)
                    <div class="col-md-8">
                        <h4>Products </h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Shop</th>
                                    <th width="120px">Price</th>
                                    <th width="50px">Count</th>
                                    <th width="120px">Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($order->products()->count() > 0)
                                    @foreach($order->products as $product)
                                        <tr>
                                            <td><a target="_blank"
                                                   href="{{url('backend/store/'.$product->store->store_id)}}#products">{{ $product->product->product_name }}</a>
                                            </td>
                                            <td><a target="_blank"
                                                   href="{{url('backend/store/'.$product->store->store_id)}}">{{ $product->store->store_name }}</a>
                                            </td>
                                            <td class="text-right"><strong>{{ $product->price }}</strong> <span
                                                        class="small text-muted">{{ $product->store->location->currency }}</span>
                                            </td>
                                            <td class="text-center">{{ $product->count }}</td>
                                            <td class="text-right">
                                                <strong>{{ ($product->price * $product->count) }}</strong> <span
                                                        class="small text-muted">{{ $product->store->location->currency }}</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                @if ($order->order_total < \App\Http\Helpers::showSetting('amount_min') )
                                    <tr>
                                        <td colspan="4"><strong>Surcharge for an order under {{ \App\Http\Helpers::showSetting('amount_min') }} CHF</strong></td>
                                        <td class="text-right"><strong>{{ \App\Http\Helpers::showSetting('commission_amount_min') }}</strong> <span
                                                    class="small text-muted">{{ $product->store->location->currency }}</span>
                                        </td>
                                        {{--<td class="text-right"></td>--}}
                                    </tr>
                                @endif
                                <tr>
                                    <td colspan="4"><strong>Commission</strong></td>
                                    <td class="text-right"><strong>{{ $order->order_comission }}</strong> <span
                                                class="small text-muted">{{ $product->store->location->currency }}</span>
                                    </td>
                                    {{--<td class="text-right"></td>--}}
                                </tr>
                                <tr>
                                    <td colspan="4"><strong>Delivery cost</strong></td>
                                    <td class="text-right"><strong>{{ $order->order_delivery }}</strong> <span
                                                class="small text-muted">{{ $product->store->location->currency }}</span>
                                    </td>
                                    {{--<td class="text-right"></td>--}}
                                </tr>
                                <tr class="info">
                                    <td colspan="4"><strong>TOTAL </strong></td>
                                    <td class="text-right">
                                        <strong>{{ $order->order_total+$order->order_delivery+$order->order_comission }}</strong>
                                        <span class="small text-muted">{{ $product->store->location->currency }}</span>
                                    </td>
                                    {{--<td class="text-right"></td>--}}
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <h4>Customer rank</h4>
                        <p>
                            <span class="glyphicon glyphicon-star{{ ($order->rating > 0 ? "" : "-empty") }}"></span>
                            <span class="glyphicon glyphicon-star{{ ($order->rating > 1 ? "" : "-empty") }}"></span>
                            <span class="glyphicon glyphicon-star{{ ($order->rating > 2 ? "" : "-empty") }}"></span>
                            <span class="glyphicon glyphicon-star{{ ($order->rating > 3 ? "" : "-empty") }}"></span>
                            <span class="glyphicon glyphicon-star{{ ($order->rating > 4 ? "" : "-empty") }}"></span>
                        </p>
                        @if ($order->order_comment != '')
                            <h4>Comment</h4>
                            <blockquote class="text-muted">{{ $order->order_comment }}</blockquote>
                        @endif
                    </div>
                @else
                    <input type="hidden" name="order[order_type]" value="1">
                    <div class="col-md-8">
                        <div class="table-responsive">
                            <h4><a href="{{ url('backend/store/'.$store->store_id) }}">{{ $store->store_name }}</a></h4>
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                @if($order->order_check != '')
                                    <tr>
                                        <td colspan="2">
                                            <a href="{{asset('img/check/'.$order->order_id."/".$order->order_check) }}"
                                               target="_blank">
                                                <img src="{{asset('img/check/'.$order->order_id."/".$order->order_check) }}"
                                                     alt="" height="200px">
                                            </a>
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <th></th>
                                    <th width="120px">Total</th>
                                </tr>

                                </thead>
                                <tbody class="js_custorm_order">
                                <tr>
                                    <td>
                                        <textarea name="order[order_text]"
                                                  class="form-control">{{ $order->order_text }}</textarea>
                                    </td>
                                    <td class="text-right"><input class="form-control" name="order[order_total]"
                                                                  value="{{ $order->order_total }}"></td>
                                </tr>
                                <tr>
                                    <td><strong>Commission</strong></td>
                                    <td class="text-right">
                                        <input type="hidden" name="order[order_comission]"
                                               value="{{ $order->order_comission }}">
                                        <strong>{{ $order->order_comission }}</strong> CHF
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Commission courier</strong></td>
                                    <td class="text-right">
                                        <input type="hidden" name="order[order_comission_courier]"
                                               value="{{ $order->order_comission_courier }}">
                                        <strong>{{ $order->order_comission_courier }}</strong> CHF
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Surcharge for an order under {{ \App\Http\Helpers::showSetting('amount_min') }} CHF</strong></td>
                                    <td class="text-right"><input class="form-control" name="order[order_surcharge]"
                                                                  value="{{ $order->order_surcharge }}"></td>
                                </tr>
                                <tr>
                                    <td><strong>Delivery cost</strong></td>
                                    <input type="hidden" name="order[order_delivery]"
                                           value="{{ $order->order_delivery }}">
                                    <td class="text-right"><strong>{{ $order->order_delivery }}</strong></td>
                                </tr>

                                <tr class="info">
                                    <td><strong>TOTAL </strong></td>
                                    <td class="text-right"><strong>{{ $order->order_total }}</strong> CHF</td>
                                </tr>
                                <tr class="js_payment {{ ($order->order_status != 3  ? 'hide' : "") }}">
                                    <td colspan="2">
                                        @if (empty($order->customer->stripe_token))
                                            <span class="text-danger">Customer`s stripe token is empty!</span>
                                        @else
                                            <a href="{{ url('backend/order/payment/'.$order->order_id) }}"
                                               class="btn btn-primary js_payment_status {{$amount == 0 ? 'disabled' : '' }}">Payment</a>
                                            <span class='text-danger js_disable_payment'>{{ $amount == 0 ? "Amount should be more null. Need Save first." : '' }} </span>
                                     @endif
                                 </td>
                             </tr>
                             </tbody>
                         </table>
                     </div>
                 </div>
             @endif
         </div>
         <button class="btn btn-primary btn-lg" type="submit">
             <span class="glyphicon glyphicon-save"></span>
             <strong> Save</strong>
         </button>
     </form>
 </div>
 @if($order->order_type == 1)
     <script>

         $(document).on('change', ".js_custorm_order input", function () {
             getCostCustomOrder(_change = 1);
         });

         $(document).on('change', "[name='order[order_status]']", function () {
             if ($(this).val() == 3) {
                 $(".js_payment").removeClass('hide');
             } else {
                 $(".js_payment").addClass('hide');
             }
         });
         getCostCustomOrder();

         function getCostCustomOrder(_change) {
             if (_change == 1){
                 $('.js_payment_status').addClass('disabled');
                 $('.js_disable_payment').text('You changed the cost. Save first');
             }
             var _order_total = $(".js_custorm_order [name='order[order_total]']").val();
             $('.js_custorm_order tr:last td.text-right strong').text(_order_total);

                _commision = _order_total * {{ $comission->setting_value }} / 100;
                _surcharge = $(".js_custorm_order tr:eq(3) input").val();
                console.log(_surcharge);
                $(".js_custorm_order tr:eq(1) input").val(_commision);
                $(".js_custorm_order tr:eq(1) td:last strong").text(_commision);
                _courier = 0;
                @if (isset($courier->setting_value))
                    _courier = _order_total * {{ $courier->setting_value }} / 100;
                @endif
                $(".js_custorm_order tr:eq(2) input").val(_courier);
                $(".js_custorm_order tr:eq(2) td:last strong").text(_courier);
                var _delivery = $("[name='order[order_delivery]']").val();
                _sum = parseFloat(_order_total) + parseFloat(_commision) + parseFloat(_courier) + parseFloat(_delivery) + parseFloat(_surcharge);
                $(".js_custorm_order tr.info td:last strong").text(_sum);
            }
        </script>
    @endif
@endsection