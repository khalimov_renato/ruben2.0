@extends('backend.app')
@section('content')
  <div class="container-fluid">
   {{ Form::open(['url' => 'backend/order', 'method' => 'GET']) }}
      {{--<input type="hidden" name="_token" value="{{ csrf_token() }}" />--}}
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <input name="search" class="form-control" type="text" value="{{ Request::get('search') }}"
                   placeholder="Type order ID or customer/courier name/email">
          </div>
        </div>
        <div class="col-md-1">
          <button class="btn btn-info" type="submit"><span class="glyphicon glyphicon-search"></span>
            <strong>Find</strong></button>
        </div>
        <div class="col-md-7 text-right"></div>
      </div>
    </form>
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
        <tr>
          <th width="50px">ID</th>
          <th width="150px">Date</th>
          <th>Customer</th>
          <th width="130px">Customer phone</th>
          <th>Delivery address</th>
          <th>Accepted</th>
          <th>Delivered</th>
          <th>Courier</th>
          <th width="70px">Comission</th>
          <th class="text-center" width="70px">Delivery cost</th>
          <th class="text-center" width="70px">Order cost</th>
          <th width="50px">Status</th>
          <th width="50px">Type</th>
          <th width="50px"></th>
        </tr>
        </thead>
        <tbody>
        @if($orders->count() == 0)
          <tr class="warning">
            <td class="text-center" colspan="12"><h3>Nothing found for your request</h3></td>
          </tr>
        @endif
        @foreach($orders as $order)
          <tr>
            <td>{{ $order->order_id }} </td>
            <td>{{ $order->created_at }}</td>
            <td>
              <a href="{{ url('backend/customer/'.$order->customer_id) }}">
                {{ $order->customer->customer_last_name }} {{ $order->customer->customer_first_name }}
              </a>
            </td>
            <td>{{ $order->customer->customer_phone }}</td>
            <td>
              <small>{{ $order->order_addr }}</small>
            </td>
            <td>
              @if(  $order->accepted_at > 0)
                 {{ $order->accepted_at }}
              @endif
            </td>
            <td>
              @if(  $order->delivered_at > 0)
                {{ $order->delivered_at }}
              @endif
            </td>
            <td>
              @if( $order->courier_id != '')
                <a href="{{ url('backend/courier/'.$order->courier_id) }}">
                  {{ $order->courier->courier_last_name }} {{ $order->courier->courier_first_name }}
                </a>
              @endif
            </td>
            <td class="text-right">
              {{ $order->order_comission }}
              <small class="text-muted">
                @if(isset($order->custom_store))
                  {{ $order->custom_store->location->currency }}
                @else
                  {{ $order->products[0]->store->location->currency }}
                @endif
              </small>
            </td>
            <td class="text-right">
              {{ $order->order_delivery }}
              <small class="text-muted">
                @if(isset($order->custom_store))
                  {{ $order->custom_store->location->currency }}
                @else
                  {{ $order->products[0]->store->location->currency }}
                @endif
              </small>
            </td>
            <td class="text-right">
              {{ $order->order_total }}
              <small class="text-muted">
                @if(isset($order->custom_store))
                  {{ $order->custom_store->location->currency }}
                @else
                  {{ $order->products[0]->store->location->currency }}
                @endif
              </small>
            </td>
            <td>
              @if ($order->order_status == \App\Order::CANCEL)
                <span class="label label-default">Cancelled </span>
              @endif
              @if ($order->order_status == \App\Order::NEW_ORDER)
                <span class="label label-danger">New</span>
              @endif
              @if ($order->order_status == \App\Order::IN_PROGRESS)
                <span class="label label-info">In progress</span>
              @endif
              @if ($order->order_status == \App\Order::DELIVERED)
                <span class="label label-warning">Delivered</span>
              @endif
              @if ($order->order_status == \App\Order::PICKED_UP)
                <span class="label label-success">Picked up</span>
              @endif
              @if ($order->order_status == \App\Order::COLLECTED)
                <span class="label label-default">Collected</span>
              @endif
              @if ($order->order_status == \App\Order::ON_THE_WAY)
                <span class="label label-info">On the way</span>
              @endif
            </td>
            <td>@if ($order->order_type == \App\Order::CANCEL)
                Default
              @else
                Custom
              @endif
            </td>
            <td>
              <a class="btn btn-primary btn-sm" role="button" href="{{ url('backend/order/'.$order->order_id) }}">
                <span class="glyphicon glyphicon-pencil"></span>
                <strong>Edit</strong>
              </a>
            </td>
          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
    <nav>
      {{ $orders->render() }}
    </nav>
  </div>
@endsection