<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport"
        content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  <title>Ruben</title>
  <link rel="shortcut icon" href="{{asset('theme/images/ico-16.png')}}" type="image/x-icon">
  <link rel="icon" href="{{asset('theme/images/ico-16.png')}}" type="image/x-icon">
  <link href="{{asset('theme/css/all-pluging.css')}}" rel="stylesheet">
  <link href="{{asset('theme/css/style.min.css')}}" rel="stylesheet">
  <link href="{{asset('theme/css/production.css')}}" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="nav-on-right">
  <noscript>
    <div class="error-js">For full functionality of this site it is necessary to enable JavaScript. Here are the <a
        href="http://enable-javascript.com">enable-javascript.com</a>.
    </div>
  </noscript>
  <!--[if lte IE 8]>
  <p class="off-js">Ваш браузер устарел, пожалуйста <b>обновите</b> его.</p>
  <![endif]-->
  <div id="page-wrapper"><!--page_wrapper start-->
    @yield('wrapper')
    <div class="hfooter"></div>
  </div><!--page_wrapper end-->
  <!--footer start-->
  <footer class="footer"></footer><!--footer end-->
  <!--script-->
  <script src="{{asset('theme/js/all-plugins.min.js')}}"></script>
  <script src="{{asset('js/jquery.maskedinput.js')}}"></script>
  <script src="{{asset('theme/js/main.js')}}"></script>
</body>
</html>