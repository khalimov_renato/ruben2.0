@extends('backend.app')

@section('content')
    @include('backend.layouts.alerts')
    <div class="container">
        <div class="row">
            <div class="col-xs-4">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Settings</h3>
                    </div>
                    <div class="panel-body">
                        {{ Form::open(['url' => 'backend/setting/', 'method' => 'POST', 'class' => 'validate form-horizontal', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product']) }}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="Comission">Comission order</label>
                                <div class="input-group">
                                    <input name="setting[comission]" type="text" class="form-control" id="Comission" placeholder="Comission order" value="{{ (isset($config['comission']) ? $config['comission'] : "") }}">
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Comission_courier">Comission courier</label>
                                <div class="input-group">
                                    <input name="setting[comission_courier]" type="text" class="form-control" id="Comission_courier" placeholder="Comission courier"
                                           value="{{ (isset($config['comission_courier']) ? $config['comission_courier'] : "") }}">
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Email">Email for requests</label>
                                <input name="setting[email]" type="text" class="form-control" id="Currency" placeholder="" value="{{ (isset($config['email']) ? $config['email'] : "") }}">
                            </div>
                            <div class="form-group">
                                <label for="min_amount">Min amount (order)</label>
                                <input name="setting[amount_min]" type="text" class="form-control" id="min_amount" placeholder="" value="{{ (isset($config['amount_min']) ? $config['amount_min'] : "") }}">
                            </div>
                            <div class="form-group">
                                <label for="commission_order">Commision (order)</label>
                                <input name="setting[commission_amount_min]" type="text" class="form-control" id="commission_order" placeholder=""
                                       value="{{ (isset($config['commission_amount_min']) ? $config['commission_amount_min'] : "") }}">
                            </div>
                            <div class="form-group">
                                <label for="google-play">GooglePlay</label>
                                <input name="setting[google_play_link]" type="text" class="form-control" id="google-play" placeholder="" value="{{ (isset($config['google_play_link']) ? $config['google_play_link'] : "") }}">
                            </div>
                            <div class="form-group">
                                <label for="app-store">AppStore</label>
                                <input name="setting[app_store_link]" type="text" class="form-control" id="app-store" placeholder=""
                                       value="{{ (isset($config['app_store_link']) ? $config['app_store_link'] : "") }}">
                            </div>
                            <button type="submit" class="btn btn-primary">
                                <span class="glyphicon glyphicon-save"></span>
                                <b>Update</b>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="col-xs-3">--}}
        {{--<div class="panel panel-warning">--}}
        {{--<div class="panel-heading">--}}
        {{--<h3 class="panel-title">SMTP & Mail</h3>--}}
        {{--</div>--}}
        {{--<div class="panel-body">--}}
        {{--<form action="{{ url('backend/setting/save') }}" method="POST">--}}
        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
        {{--<div class="form-group">--}}
        {{--<label for="smtp_host">SMTP Host</label>--}}
        {{--<input name="setting[smtp_host]" type="text" class="form-control" id="smtp_host" placeholder="SMTP Host"--}}
        {{--value="{{ (isset($config['smtp_host']) ? $config['smtp_host'] : "") }}">--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
        {{--<label for="smtp_port">SMTP Port</label>--}}
        {{--<input name="setting[smtp_port]" type="text" class="form-control" id="smtp_port" placeholder="SMTP Port"--}}
        {{--value="{{ (isset($config['smtp_port']) ? $config['smtp_port'] : "") }}">--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
        {{--<label for="smtp_login">SMTP Login</label>--}}
        {{--<input name="setting[smtp_login]" type="text" class="form-control" id="smtp_login"--}}
        {{--placeholder="SMTP Login" value="{{ (isset($config['smtp_login']) ? $config['smtp_login'] : "") }}">--}}
        {{--</div>--}}
        {{--<div class="form-group">--}}
        {{--<label for="smtp_pass">SMTP Password</label>--}}
        {{--<input name="setting[smtp_pass]" type="password" class="form-control" id="smtp_pass"--}}
        {{--placeholder="SMTP password"--}}
        {{--value="{{ (isset($config['smtp_pass']) ? $config['smtp_pass'] : "") }}">--}}
        {{--</div>--}}
        {{--<button type="submit" class="btn btn-default">Submit</button>--}}
        {{--</form>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
    </div>
@endsection