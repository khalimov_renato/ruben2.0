@extends('backend.app')
@section('content')
    @include('backend.layouts.alerts')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>
                    <a href="{{ url('backend/city/create') }}" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"></span> <b>Add new city</b></a>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(session('status') != '')
                    <div class="alert alert-success">
                        {{ session('status') }}
                        <span class="glyphicon glyphicon-remove pull-right js_close_alert"></span>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-stripped table-bordered table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Aliases</th>
                            <th width="100px">Currency</th>
                            <th width="50px">Status</th>
                            <th width="150px"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cities as $c)
                            <tr class="js_item_area">
                                <td>{{$c->name}}</td>
                                <td>{{$c->alias }}</td>
                                <td class="text-center">{{$c->currency}}</td>
                                <td>
                                    @if ($c->active == 1)
                                        <span class="label label-success">Active </span>
                                    @else
                                        <span class="label label-danger">Disabled</span>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-primary btn-sm" href="{{ url('backend/city/' . $c->id . '/edit') }}">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                        <strong>Edit</strong>
                                    </a>
                                    <a class="btn btn-danger btn-sm js_delete" href="#">
                                        <span class="glyphicon glyphicon-trash"></span>
                                        <strong>Delete</strong>
                                    </a>
                                    <div class="hidden">
                                        {!! Form::open(['route' => ['city.destroy', $c->id], 'method' => 'delete']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger js_delete', 'onclick' => "return confirm('Are you sure?)"]) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav>
                    {{ $cities->appends(['search' => request()])->render() }}
                </nav>

            </div>
        </div>
    </div>
@endsection