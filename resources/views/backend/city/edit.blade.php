@extends('backend.app')
@section('content')
    @include('backend.layouts.alerts')
    <div class="container">

        {{ Form::open(['url' => 'backend/city/' . $city->id, 'method' => 'PUT', 'class' => 'validate form-horizontal', "novalidate" => 'novalidate', 'autocomplete' => 'off', 'id' => 'product']) }}
        {{ Form::hidden('id', $city->id) }}
        <div class="row">
            <div class="col-md-4">
                <h4>City information</h4>
                <div class="form-group">
                    <label class="control-label">Name</label>
                    {{  Form::text('name', $city->name, ['class' => 'form-control', 'required' => 'required',  "placeholder" => '']) }}
                </div>
                <div class="form-group">
                    <label class="control-label">Aliases</label>
                    {{  Form::text('alias', $city->alias, ['class' => 'form-control', 'required' => 'required',  "placeholder" => 'comma separated']) }}
                </div>
                <div class="form-group">
                    <label class="control-label">Currency </label>
                    {{  Form::text('currency', $city->currency, ['class' => 'form-control', 'maxlength' => 3, 'required' => 'required',  "placeholder" => 'EUR']) }}
                </div>
                <div class="form-group">
                    <label class="control-label">
                        Active &nbsp;
                        <div class="pull-right">
                            <div class="btn-group" role="group">
                                <input name="active" value="1" class="js_check" type="checkbox"
                                       @if(isset($city->active) && $city->active) checked @endif >
                            </div>
                        </div>
                    </label>
                </div>
            </div>

            <div class="col-md-8">

                <table id="js_table" class="table table-bordered table-striped table-condensed">
                    <thead>
                    <tr>
                        <th colspan="2">ZIP</th>
                        <th rowspan="2">Distance</th>
                        <th rowspan="2">Cost</th>
                        <th width="32px" rowspan="2"></th>
                    </tr>
                    <tr>
                        <th>Shop</th>
                        <th>Customer</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($city->deliveries)
                        @foreach($city->deliveries as $delivery)
                            <tr>
                                <td><input type="text" required name="delivery[shop_zip][]" value="{{ $delivery->shop_zip }}" class="form-control input-sm"></td>
                                <td><input type="text" required name="delivery[customer_zip][]" value="{{ $delivery->customer_zip }}" class="form-control input-sm"></td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" required class="form-control input-sm" value="{{ $delivery->distance }}" name="delivery[distance][]" aria-describedby="basic-addon2">
                                        <span class="input-group-addon" id="basic-addon2">km</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" required class="form-control input-sm" value="{{ $delivery->cost }}" name="delivery[cost][]" aria-describedby="basic-addon2">
                                        <span class="input-group-addon" id="basic-addon2">{{$city->currency or ''}}</span>
                                    </div>
                                </td>
                                <td>
                                    <a href="#" class="js_remove btn btn-sm btn-danger">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    <tr>
                        <td><input type="text" required name="delivery[shop_zip][]" id="" class="form-control input-sm"></td>
                        <td><input type="text" required name="delivery[customer_zip][]" id="" class="form-control input-sm"></td>
                        <td>
                            <div class="input-group">
                                <input type="text" required class="form-control input-sm" name="delivery[distance][]" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">km</span>
                            </div>
                        </td>
                        <td>
                            <div class="input-group">
                                <input type="text" required class="form-control input-sm" name="delivery[cost][]" aria-describedby="basic-addon2">
                                <span class="input-group-addon" id="basic-addon2">{{$city->currency or ''}}</span>
                            </div>
                        </td>
                        <td>
                            <a href="#" class="js_remove btn btn-sm btn-danger">
                                <span class="glyphicon glyphicon-remove"></span>
                            </a>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="5" class="text-center">
                            <a href="#" id="js_clone_zip" class="btn btn-success">
                                <span class="glyphicon glyphicon-plus-sign"></span>
                                <b>Add new zip sequence</b>
                            </a>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <button class="btn btn-primary btn-lg" type="submit">
            <span class="glyphicon glyphicon-save"></span>
            <strong> Save</strong>
        </button>
        </form>
    </div>

@endsection


@section('js')
    <script>
        function check_uniq() {
            var _set = [];
            $("#js_table tbody tr").each(function () {
                _set.push($(this).find("input:eq(0)").val() + $(this).find("input:eq(1)").val());
            })
            _uniq = [];
            _idx  = [];
            for (i in _set) {
                if ($.inArray(_set[i], _uniq) === -1)
                    _uniq.push(_set[i]);
                else
                    _idx.push(i);
            }
            if (_uniq.length < _set.length) {
                $("#js_table tbody tr").addClass("danger");
                for (i in _idx)
                    $("#js_table tbody tr:eq(" + (_idx[i] - 1) + ")").removeClass("danger")

            }
            return _idx.length;
        }

        $(document).ready(function () {
            $("#js_clone_zip").on('click', function () {
                _body = $(this).closest('table').find('tbody');
                _row  = _body.find('tr:first').clone();
                _body.append(_row.clone());
                return false;
            })
            $('table').on('click', '.js_remove', function () {
                $(this).closest('tr').remove();
                return false;
            })
            $('[type=submit]').on('click', function () {
                _check = check_uniq();
                if (_check > 0)
                    alert(_check + ' duplicates found');
                return (_check == 0);
            })
        })
    </script>
@endsection