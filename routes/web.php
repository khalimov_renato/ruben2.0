<?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

    Route::group(['middleware' => 'web'], function () {
        /*php
        |--------------------------------------------------------------------------
        |  BackEnd
        |--------------------------------------------------------------------------
        */
        Route::group(['prefix' => 'backend'], function () {

            /**
             * Авторизация
             */
            Route::group(['middleware' => ['guest']], function () {
                Route::get('/', 'Auth\LoginController@loginForm')->name('login');
                Route::post('/', 'Auth\LoginController@login')->name('auth');
            });

            Route::group(['middleware' => ['auth']], function () {
                Route::get('/logout', 'Auth\LoginController@logout');
                Route::resource('order', 'Backend\Order\OrderController');
                Route::resource('city', 'Backend\City\CityController');
                Route::resource('setting', 'Backend\Setting\SettingController');
                Route::get('overview', 'Backend\Overview\OverviewController@index');
            });

        });
    });